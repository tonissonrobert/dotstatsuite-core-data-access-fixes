--Create role if it does not exist yet
IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE TYPE = 'R' AND name = 'DotStatWriter')
BEGIN
   CREATE ROLE [DotStatWriter];
END
GO

--Add role to datareader and datawriter roles
ALTER ROLE [db_datareader] ADD MEMBER [DotStatWriter];
ALTER ROLE [db_datawriter] ADD MEMBER [DotStatWriter];

--Grant execute permission to role
GRANT EXECUTE ON SCHEMA::[dbo] TO [DotStatWriter];

IF EXISTS (SELECT 1 FROM sys.database_principals WHERE TYPE = 'S' AND name = '$loginName$')
BEGIN    
	--Add user to role
	ALTER ROLE [DotStatWriter] ADD MEMBER [$loginName$];

	--Remove user from datareader, datawriter and ddladmin roles in case it was added to them by a previous version of DbUp
	ALTER ROLE [db_datareader] DROP MEMBER [$loginName$];
	ALTER ROLE [db_datawriter] DROP MEMBER [$loginName$];
END
