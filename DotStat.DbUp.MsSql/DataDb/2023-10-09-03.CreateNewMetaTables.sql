SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL ON
GO
 
DECLARE 	
	@DSD_ID int,
	@DF_ID int,
	@MSD_ID int,
	@Compression varchar(20),
	@SDMX_ID nvarchar(1000),
	@msg nvarchar(1000),
	@table_version char(1),	
    @NewLineChar AS CHAR(2) =  CHAR(10),
	@U_DF_VERSION_ID varchar(20),
	@U_DSD_VERSION_ID varchar(20),
	@Transaction_ID int,
	@SQL NVARCHAR(MAX),
	@PkColumns NVARCHAR(MAX);

DECLARE
	@DSD_IDS TABLE(ART_ID int PRIMARY KEY, MSD_ID int, SDMX_ID nvarchar(1000), COMPRESSION nvarchar(50));
	
DECLARE
	@DF_IDS TABLE(ART_ID int PRIMARY KEY, SDMX_ID nvarchar(1000));

DECLARE 
	@VERSIONS TABLE ([version] char(1));

INSERT into @DSD_IDS 
	SELECT ART_ID, MSD_ID, [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')', [DATA_COMPRESSION] 
	FROM [management].[ARTEFACT]
	WHERE [type] = 'DSD' AND MSD_ID IS NOT NULL ORDER BY ART_ID
	
--------------------------------------------------------
--Create new transaction for DSD
SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		
INSERT 
	INTO [management].[DSD_TRANSACTION]([TRANSACTION_ID], [ART_ID], [TABLE_VERSION], [EXECUTION_START], [EXECUTION_END], [SUCCESSFUL], [ARTEFACT_FULL_ID], [USER_EMAIL])
	VALUES (@transaction_ID, -1, 'X', GETDATE(), GETDATE(), 0, 'N/A', NULL)
--------------------------------------------------------
	
WHILE (Select Count(*) FROM @DSD_IDS) > 0
BEGIN

  SELECT TOP 1 @DSD_ID = ART_ID, @MSD_ID = MSD_ID, @Compression = COMPRESSION FROM @DSD_IDS
	DELETE @DSD_IDS WHERE ART_ID = @DSD_ID
	
--------------------------------------------------------
	DELETE @VERSIONS
	INSERT into @VERSIONS SELECT 'A' union all SELECT 'B'
--------------------------------------------------------

	BEGIN TRY
		WHILE (Select Count(*) FROM @VERSIONS) > 0
		BEGIN
			-- select next version
			SELECT top 1 @table_version = [version] from @VERSIONS
			DELETE FROM @VERSIONS where [version]=@table_version

			BEGIN TRAN
				INSERT into @DF_IDS 
				select ART_ID, [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')' 	
				from management.ARTEFACT where [TYPE]='DF' and DF_DSD_ID=@DSD_ID

				WHILE (Select Count(*) FROM @DF_IDS) > 0
				BEGIN
					SELECT TOP 1 @DF_ID = ART_ID FROM @DF_IDS
					DELETE @DF_IDS WHERE ART_ID = @DF_ID
					SET @U_DF_VERSION_ID = CAST(@DF_ID AS VARCHAR) + '_' + @table_version;
					SET @U_DSD_VERSION_ID = CAST(@DSD_ID AS VARCHAR) + '_' + @table_version;

					IF NOT EXISTS(SELECT 1 FROM sys.tables WHERE Object_ID = Object_ID(N'data.META_DF_' + @U_DF_VERSION_ID))
					BEGIN	
                        
						--	CREATE NEW TABLE data.META_DF_{DF_ID}_A/B based on the definition of data.META_{DSD_ID}_A/B
						SET @SQL = 'SELECT TOP (0) * ' + @NewLineChar; 
						SET @SQL += 'INTO [data].[META_DF_' + @U_DF_VERSION_ID +']' + @NewLineChar;
						SET @SQL += 'FROM [data].[META_' + @U_DSD_VERSION_ID +']' + @NewLineChar;
						
						--PRINT @SQL;
						exec sp_executesql @SQL;


						IF(@Compression = 'NONE')
						BEGIN
							--Get Primary Key columns
							SET @PkColumns = ISNULL(STUFF(( 
								SELECT ', [' +  COLUMN_NAME + ']'
								FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
								WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
								AND TABLE_NAME = 'META_' + @U_DSD_VERSION_ID AND TABLE_SCHEMA = 'data'  
								FOR XML PATH('')
							), 1, 1, ''), '') 

							--Create primary key
							SET @SQL = 'ALTER TABLE [data].[META_DF_' + @U_DF_VERSION_ID +']' + @NewLineChar;
							SET @SQL += 'ADD CONSTRAINT [PK_META_DF_' + @U_DF_VERSION_ID +'] PRIMARY KEY('+ @PkColumns +');' + @NewLineChar;						
						END
						ELSE BEGIN
							--Create CLUSTERED COLUMNSTORE INDEX
							SET @SQL = 'CREATE CLUSTERED COLUMNSTORE INDEX [CCI_META_DF_' + @U_DF_VERSION_ID +'] ON [data].[META_DF_' + @U_DF_VERSION_ID +'] WITH(DATA_COMPRESSION = ' + @Compression +')' + @NewLineChar;
						END
						--PRINT @SQL;
						exec sp_executesql @SQL;

					END
				END
			COMMIT TRAN
		END		
	END TRY  
	BEGIN CATCH 
		IF (XACT_STATE()) <> 0 ROLLBACK TRAN
						
		SELECT @msg= 'The following error was found while trying to create the new MSD DF TABLES ['+ @SDMX_ID +'] (' + @U_DSD_VERSION_ID + '):'
			+ @NewLineChar + 'ErrorLine:' + CAST(ERROR_LINE() AS VARCHAR)
			+ @NewLineChar + 'ErrorNumber:' + CAST(ERROR_NUMBER() AS VARCHAR)
			+ @NewLineChar + 'ErrorMessage:' + ERROR_MESSAGE()+@NewLineChar 
		
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2023-10-09-03.CreateNewMetaTables.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
				
	END CATCH

END

--update transaction info
UPDATE  [management].[DSD_TRANSACTION] SET [EXECUTION_END] = GETDATE(), [SUCCESSFUL] = 1
WHERE [TRANSACTION_ID] = @Transaction_ID