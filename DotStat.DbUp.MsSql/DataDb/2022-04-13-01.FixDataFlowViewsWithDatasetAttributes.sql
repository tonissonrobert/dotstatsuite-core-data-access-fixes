﻿SET NOCOUNT ON;
SET CONCAT_NULL_YIELDS_NULL ON;
GO

DECLARE
   @sql nvarchar(max),
   @df_id int,
   @table_versions char(2) = 'AB',
   @current_version char(1) = '',
   @i int,
   @transaction_ID int,
   @msg varchar(max),
   @current_view varchar(1000),
   @current_dataflow varchar(1000),
   @views_updated varchar(max) = '',
   @views_skipped varchar(max) = '',
   @views_with_error varchar(max) = '';

DECLARE dataflow_cursor CURSOR FOR
   SELECT a.ART_ID
     FROM [management].[ARTEFACT] a
    WHERE a.[type] = 'DF' 
      AND EXISTS (SELECT * FROM [management].[COMPONENT] c WHERE c.DSD_ID = a.DF_DSD_ID AND c.ATT_ASS_LEVEL = 'DataSet');

--Create new transaction for the log entries of the script
SELECT @transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];

INSERT 
  INTO [management].[DSD_TRANSACTION]
       ([TRANSACTION_ID], [ART_ID], [TABLE_VERSION], [EXECUTION_START], [EXECUTION_END], [SUCCESSFUL], [ARTEFACT_FULL_ID], [USER_EMAIL])
VALUES (@transaction_ID, -1, 'X', GETDATE(), GETDATE(), 0, 'N/A', NULL)

SET @msg = 'Started upgrading views of dataflows having dataset attributes.'
INSERT
  INTO [management].[LOGS]
       ([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
VALUES (@Transaction_ID, GETDATE(), 'INFO', 'MSSQL SERVER:' + @@SERVERNAME, '2022-04-13-01.FixDataFlowViewsWithDatasetAttributes.sql', @msg , NULL, 'dotstatsuite-dbup');

OPEN dataflow_cursor  
FETCH NEXT FROM dataflow_cursor INTO @df_id

WHILE @@FETCH_STATUS = 0
BEGIN  
   SET @i = 0;

   WHILE @i < 2
   BEGIN
      SET @i += 1;
      SET @current_version = SUBSTRING(@table_versions, @i, 1);
      SET @current_view = 'VI_CurrentDataDataFlow_' + CONVERT(varchar, @df_id) + '_' + @current_version;

      SELECT @sql = definition
        FROM sys.objects     o
        JOIN sys.sql_modules m ON m.object_id = o.object_id
       WHERE o.object_id = object_id( '[data].[' + @current_view + ']')
         AND o.type = 'V';

      -- Update view if exists and hasn't been updated yet
      IF (@sql IS NOT NULL AND CHARINDEX('SELECT TOP 1 [ADF].[COMP', @sql) = 0)
      BEGIN
         -- Update view
         SET @sql = REPLACE(@sql, 'SELECT [ADF].[COMP', 'SELECT TOP 1 [ADF].[COMP');

         IF (CHARINDEX(' ALTER ', @sql) = 0)
         BEGIN
            DECLARE @pos int = CHARINDEX('CREATE ', @sql);

            -- Replace CREATE with ALTER if not yet present is command text
            IF (@pos > 0)
               SET @sql = STUFF(@sql, @pos, 7, 'ALTER ');
         END

         BEGIN TRY
            --PRINT @sql

            EXEC sp_executesql @sql

            SET @views_updated += @current_view + ',';
         END TRY
         BEGIN CATCH
            SET @views_with_error += @current_view + ',';

            SELECT @current_dataflow = [AGENCY] + ':' + [ID] + '(' + CAST([VERSION_1] AS VARCHAR) + '.' + CAST([VERSION_2] AS VARCHAR) + CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.' + CAST([VERSION_3] AS VARCHAR) END +')'
               FROM [management].[ARTEFACT] 
               WHERE [ART_ID] = @df_id AND [TYPE] = 'DF'

            SET @msg = 'The following error was found while trying to create the dataFlow view [' + @current_view + '] of dataflow ['+ @current_dataflow +'] (The creation of the view will be skiped):' + ' ErrorNumber:'+CAST(ERROR_NUMBER() AS VARCHAR) + ' ErrorMessage:'+ERROR_MESSAGE();

            -- Log error message
            PRINT @msg
            INSERT
               INTO [management].[LOGS] 
                     ([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
            VALUES (@Transaction_ID, GETDATE(), 'ERROR', 'MSSQL SERVER:' + @@SERVERNAME, '2022-04-13-01.FixDataFlowViewsWithDatasetAttributes.sql', @msg, ERROR_MESSAGE(), 'dotstatsuite-dbup')
         END CATCH
      END -- IF (@sql IS NOT NULL AND CHARINDEX('SELECT TOP 1 [ADF].[COMP', @sql) = 0)
      ELSE
         SET @views_skipped += @current_view + ',';
      END

   FETCH NEXT FROM dataflow_cursor INTO @df_id
END

CLOSE dataflow_cursor
DEALLOCATE dataflow_cursor

SET @msg = 'Finished upgrading views of dataflows having dataset attributes.'
   INSERT
     INTO [management].[LOGS]
          ([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
   VALUES (@Transaction_ID, GETDATE(), 'INFO', 'MSSQL SERVER:' + @@SERVERNAME, '2022-04-13-01.FixDataFlowViewsWithDatasetAttributes.sql', @msg , NULL, 'dotstatsuite-dbup');

IF (@views_skipped <> '')
BEGIN
   SET @msg = 'The following dataflow views were skipped as they had been already updated: ' + TRIM(',' FROM @views_skipped);
   INSERT
     INTO [management].[LOGS]
          ([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
   VALUES (@Transaction_ID, GETDATE(), 'INFO', 'MSSQL SERVER:' + @@SERVERNAME, '2022-04-13-01.FixDataFlowViewsWithDatasetAttributes.sql', @msg , NULL, 'dotstatsuite-dbup');
END

IF (@views_updated <> '')
BEGIN
   SET @msg = 'The following dataflow views have been updated: ' + TRIM(',' FROM @views_updated);
   INSERT
     INTO [management].[LOGS]
          ([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
   VALUES (@Transaction_ID, GETDATE(), 'INFO', 'MSSQL SERVER:' + @@SERVERNAME, '2022-04-13-01.FixDataFlowViewsWithDatasetAttributes.sql', @msg , NULL, 'dotstatsuite-dbup');
END

IF (@views_with_error <> '')
BEGIN
   SET @msg = 'The following dataflow views have failed to be updated: ' + TRIM(',' FROM @views_with_error);
   INSERT
     INTO [management].[LOGS]
          ([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
   VALUES (@Transaction_ID, GETDATE(), 'INFO', 'MSSQL SERVER:' + @@SERVERNAME, '2022-04-13-01.FixDataFlowViewsWithDatasetAttributes.sql', @msg , NULL, 'dotstatsuite-dbup');
END

--update transaction info
UPDATE  [management].[DSD_TRANSACTION]
   SET [EXECUTION_END] = GETDATE(), [SUCCESSFUL] = 1
 WHERE [TRANSACTION_ID] = @Transaction_ID

