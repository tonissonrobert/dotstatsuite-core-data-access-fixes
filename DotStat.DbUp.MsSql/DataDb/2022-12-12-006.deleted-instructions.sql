SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL ON
GO
 
DECLARE 	
	@DSD_ID int,
	@SDMX_ID nvarchar(1000),

	@sql_migration NVARCHAR(MAX),	
	@msg nvarchar(1000),

	@table_version char(1),	
    @NewLineChar AS CHAR(2) =  CHAR(10),
	@U_DSD_VERSION_ID varchar(20),
	@Transaction_ID int,
	@HasTimeDim bit;

DECLARE
	@DSD_Components TABLE(
		COMP_ID int not null,
		ID varchar(50) not null, 
		[TYPE] varchar(50) not null, 	
		CL_ID int, 
		ATT_ASS_LEVEL varchar(50),
		ATT_STATUS varchar(11)
	);
	
DECLARE
	@DSD_IDS TABLE(ART_ID int PRIMARY KEY, SDMX_ID nvarchar(1000));

DECLARE 
	@VERSIONS TABLE ([version] char(1));

INSERT into @DSD_IDS 
	SELECT ART_ID, [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')' 
	FROM [management].[ARTEFACT]
	WHERE [type] = 'DSD' ORDER BY ART_ID

--------------------------------------------------------
--Create new transaction for DSD
SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		
INSERT 
	INTO [management].[DSD_TRANSACTION]([TRANSACTION_ID], [ART_ID], [TABLE_VERSION], [EXECUTION_START], [EXECUTION_END], [SUCCESSFUL], [ARTEFACT_FULL_ID], [USER_EMAIL])
	VALUES (@transaction_ID, -1, 'X', GETDATE(), GETDATE(), 0, 'N/A', NULL)
--------------------------------------------------------
	
WHILE (Select Count(*) FROM @DSD_IDS) > 0
BEGIN

    SELECT TOP 1 @DSD_ID=ART_ID, @SDMX_ID=SDMX_ID FROM @DSD_IDS
	DELETE @DSD_IDS WHERE ART_ID = @DSD_ID
	
	print '';
	print 'Migrating DSD: ' + @SDMX_ID + ' ,ID:' + CAST(@DSD_ID AS VARCHAR);

	-- Retrieve IF DSD has TIME dimension
	SELECT @HasTimeDim=1 FROM [management].[COMPONENT] WHERE [TYPE] ='TimeDimension' AND DSD_ID = @DSD_ID 

	-- Get dimension components
	DELETE @DSD_Components;

	INSERT INTO @DSD_Components
	SELECT c.COMP_ID,c.ID,c.[TYPE],c.[CL_ID],c.[ATT_ASS_LEVEL], c.[ATT_STATUS]
	FROM [management].[COMPONENT] c
	WHERE c.DSD_ID=@DSD_ID and [Type] in ('Dimension','Attribute')

--------------------------------------------------------
	DELETE @VERSIONS
	INSERT into @VERSIONS SELECT 'A' union all SELECT 'B'
--------------------------------------------------------

	BEGIN TRY
		WHILE (Select Count(*) FROM @VERSIONS) > 0
		BEGIN
			-- select next version
			SELECT top 1 @table_version = [version] from @VERSIONS
			DELETE FROM @VERSIONS where [version]=@table_version

			SET @U_DSD_VERSION_ID = CAST(@DSD_ID AS VARCHAR) + '_' + @table_version;

			IF EXISTS(SELECT 1 FROM sys.tables WHERE Object_ID = Object_ID(N'data.DELETED_' + @U_DSD_VERSION_ID))
			BEGIN
				print @U_DSD_VERSION_ID + ' already migrated, skipped';
				CONTINUE
			END
			
			BEGIN TRAN

			-- Drop data.FACT_{ID}_Deleted table 
			IF EXISTS(SELECT 1 FROM sys.tables WHERE [object_id]=Object_ID(N'data.FACT_' + @U_DSD_VERSION_ID + '_DELETED'))
			BEGIN
				set @sql_migration = 'DROP table data.FACT_' + @U_DSD_VERSION_ID + '_DELETED'
				exec sp_executesql @sql_migration;
			END

			SET @sql_migration = 'CREATE TABLE [data].DELETED_' + @U_DSD_VERSION_ID + '([DF_ID] int not null' + @NewLineChar
			SET @sql_migration += CASE @HasTimeDim
						WHEN 1 THEN ',[PERIOD_SDMX] varchar(30), [PERIOD_START] datetime2(0), [PERIOD_END] datetime2(0)' + @NewLineChar
						ELSE ''
					END

			SET @sql_migration += ISNULL(STUFF(( 
					SELECT ',' +
						CASE 
						-- Coded Dimensions
						WHEN C.[TYPE] = 'Dimension'
							THEN '[DIM_' + CAST(C.[COMP_ID] AS VARCHAR) + '] int' + @NewLineChar
						-- Attributes
						WHEN C.[TYPE] = 'Attribute'
							THEN '[COMP_' + CAST(C.[COMP_ID] AS VARCHAR)+'] char(1)' + @NewLineChar
						END
					FROM @DSD_Components C  FOR XML PATH('')
				), 1, 0, ''), '');
			
			SET @sql_migration += ',[VALUE] char(1)' + @NewLineChar
			SET @sql_migration += ',[LAST_UPDATED] [datetime] NOT NULL DEFAULT (GETDATE()))'

			exec sp_executesql @sql_migration;

			SET @sql_migration = 'CREATE NONCLUSTERED INDEX I_DELETED_' + @U_DSD_VERSION_ID + ' ON [data].DELETED_' + @U_DSD_VERSION_ID + '([DF_ID],[LAST_UPDATED])';

			exec sp_executesql @sql_migration;

			-- create delete view
			exec dbo.CREATE_DEL_VIEWS @DSD_ID, @table_version; 
			
			-- recreate dsd view
			exec dbo.CREATE_DSD_VIEW @DSD_ID, @table_version; 
			
			-- recreate df views
			exec dbo.CREATE_DF_VIEWS @DSD_ID, @table_version; 

			commit TRAN
		END		
	END TRY  
	BEGIN CATCH 
		IF (XACT_STATE()) <> 0 ROLLBACK TRAN
						
		SELECT @msg= 'The following error was found while trying to migrate DSD ['+ @SDMX_ID +'] (' + @U_DSD_VERSION_ID + '):'
			+ @NewLineChar + 'ErrorLine:' + CAST(ERROR_LINE() AS VARCHAR)
			+ @NewLineChar + 'ErrorNumber:' + CAST(ERROR_NUMBER() AS VARCHAR)
			+ @NewLineChar + 'ErrorMessage:' + ERROR_MESSAGE()+@NewLineChar 
		
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2022-12-12-005.deleted-instructions.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
				
	END CATCH

END

--update transaction info
UPDATE  [management].[DSD_TRANSACTION] SET [EXECUTION_END] = GETDATE(), [SUCCESSFUL] = 1
WHERE [TRANSACTION_ID] = @Transaction_ID