﻿SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL ON
GO
  
DECLARE 	
	@DSD_ID int,
	@DF_ID int,
	@sql_migration NVARCHAR(MAX),	
	@sql_view NVARCHAR(MAX),
	@sql_select NVARCHAR(MAX),
	@sql_from NVARCHAR(MAX),
	@sql_join NVARCHAR(MAX),
	@table_version char(1),	
    @NewLineChar AS CHAR(2) =  CHAR(10),
	@U_DSD_VERSION_ID varchar(20),
	@U_DF_VERSION_ID varchar(20),
	@Transaction_ID int,
	@HasDataSetAttributes as bit;

DECLARE 
	@VERSIONS TABLE ([version] char(1));
	
DECLARE
	@DSD_IDS TABLE(ART_ID int PRIMARY KEY);

DECLARE
	@DF_IDS TABLE(ART_ID int PRIMARY KEY);

INSERT into @DSD_IDS 
	SELECT ART_ID FROM [management].[ARTEFACT]
	WHERE [type] = 'DSD' ORDER BY ART_ID

--------------------------------------------------------
--Create new transaction for DSD
SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		
INSERT 
	INTO [management].[DSD_TRANSACTION]([TRANSACTION_ID], [ART_ID], [TABLE_VERSION], [EXECUTION_START], [EXECUTION_END], [SUCCESSFUL], [ARTEFACT_FULL_ID], [USER_EMAIL])
	VALUES (@transaction_ID, -1, 'X', GETDATE(), GETDATE(), 0, 'N/A', NULL)
--------------------------------------------------------
	
WHILE (Select Count(*) FROM @DSD_IDS) > 0
BEGIN

    SELECT TOP 1 @DSD_ID = ART_ID FROM @DSD_IDS
	DELETE @DSD_IDS WHERE ART_ID = @DSD_ID
	
	print 'Migrating DSD: ' + CAST(@DSD_ID AS VARCHAR)
		
    SELECT c.DSD_ID, c.ID, c.CL_ID, c.COMP_ID,c.[TYPE], c.ATT_ASS_LEVEL, c.ATT_STATUS
    INTO #Temp_DSD_Components
    FROM [management].[ARTEFACT] a 
    INNER JOIN [management].[COMPONENT] c 
    ON a.ART_ID = c.DSD_ID
    WHERE A.ART_ID=@DSD_ID
	
	--Has DataSet level attributes
	SELECT @HasDataSetAttributes = 0
	IF EXISTS(SELECT TOP 1 1 FROM #Temp_DSD_Components C WHERE C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'DataSet')
	BEGIN
		SELECT @HasDataSetAttributes=1
	END

	--reset values
	SELECT @sql_select = 'SELECT [SID]'+ COALESCE( STUFF(( 
		SELECT 
			CASE 
			--Non-coded dataset level attributes
			WHEN C.[CL_ID] IS NULL AND C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'DataSet'
				THEN ', [ADF].[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+']'
			--coded dataset level attributes
			WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'DataSet'
				THEN ', [CL_'+[C].[ID]+'].[ID] AS ['+[C].[ID]+']'
			ELSE
				', ['+[C].[ID]+']'
			END
		FROM #Temp_DSD_Components C FOR XML PATH('')
	), 1, 0, ''), '') 
				
	-- Has Time dimension
	IF EXISTS(SELECT 1 FROM sys.columns 
			WHERE Name = N'PERIOD_SDMX'
			AND Object_ID = Object_ID(N'[data].[FACT_'+CAST(@DSD_ID AS VARCHAR)+'_A]'))
	BEGIN
		SELECT @sql_select = @sql_select + ', PERIOD_START, PERIOD_END' + @NewLineChar ;
	END

--------------------------------------------------------
	DELETE @VERSIONS
	INSERT into @VERSIONS SELECT 'A' union all SELECT 'B'
--------------------------------------------------------

	BEGIN TRY
		WHILE (Select Count(*) FROM @VERSIONS) > 0
		BEGIN
		
			-- select next version
			SELECT top 1 @table_version = [version] from @VERSIONS
			DELETE FROM @VERSIONS where [version]=@table_version


			-- Handle dataflow views
			DELETE @DF_IDS;
			INSERT into @DF_IDS 
			select ART_ID from management.ARTEFACT where [TYPE]='DF' and DF_DSD_ID=@DSD_ID
			
			SET @U_DSD_VERSION_ID = CAST(@DSD_ID AS VARCHAR) + '_' + @table_version;

			WHILE (Select Count(*) FROM @DF_IDS) > 0
			BEGIN
				SELECT TOP 1 @DF_ID = ART_ID FROM @DF_IDS
				DELETE @DF_IDS WHERE ART_ID = @DF_ID

				--Join to codelist tables
				SELECT @sql_join = COALESCE( STUFF(( 
					SELECT 
						'LEFT JOIN [management].[CL_'+CAST(CL_ID AS VARCHAR)+'] AS [CL_'+[ID]+'] ON [ADF].[DF_ID] = ' + CAST(@DF_ID AS VARCHAR) + ' AND ADF.COMP_'+CAST(COMP_ID AS VARCHAR)+' = [CL_'+[ID]+'].[ITEM_ID]'+@NewLineChar --CODED
					FROM #Temp_DSD_Components C WHERE C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'DataSet' AND C.[CL_ID] IS NOT NULL 	
					FOR XML PATH('')
				), 1, 0, ''), '')

				SET @U_DF_VERSION_ID = CAST(@DF_ID AS VARCHAR) + '_' + @table_version;
				
				print 'Migrating DF: ' + @U_DF_VERSION_ID;

				select @sql_view = 'CREATE OR ALTER VIEW [data].[VI_CurrentDataDataFlow_'+ @U_DF_VERSION_ID+'] AS '+ @NewLineChar; 

				-- Fix bug, dataSet level views seem to allways use the A version of the table 'ATTR_{DSD_ID}_{TABLE_VERSION}_DF'
				SELECT @sql_view = REPLACE(@sql_view, 'FROM [data].[ATTR_'+CAST(@DSD_ID AS VARCHAR)+'_A', 'FROM [data].[ATTR_'+@U_DSD_VERSION_ID);
				
				SELECT @sql_from = 'FROM [data].VI_CurrentDataDsd_'+@U_DSD_VERSION_ID + @NewLineChar; 
					
				IF (@HasDataSetAttributes = 1)
				BEGIN
					SELECT @sql_from = @sql_from + 'FULL JOIN [data].[ATTR_'+@U_DSD_VERSION_ID+'_DF] ADF ON DF_ID = '+CAST(@DF_ID AS VARCHAR) + @NewLineChar; 
				END				
				
				SELECT @sql_view = @sql_view + @sql_select + @sql_from + @sql_join
				--PRINT @sql_view
				exec sp_executesql @sql_view
								
			END		
		END
	END TRY  
	BEGIN CATCH  		
		DECLARE 
			@DSD nvarchar(100),
			@msg nvarchar(1000);
			
		SELECT @DSD  = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+'.'+ CASE WHEN [VERSION_3]IS NULL THEN '0' ELSE CAST([VERSION_3] AS VARCHAR) END +')'
		FROM [management].[ARTEFACT] 
		WHERE [ART_ID] = @DSD_ID AND [TYPE] = 'DSD'
			
		SELECT @msg= 'The following error was found while trying to update Views for the DSD ['+ @DSD +'] (The update of the view will be skiped):'+ ' ErrorNumber:'+CAST(ERROR_NUMBER() AS VARCHAR)+' ErrorMessage:'+ERROR_MESSAGE()+@NewLineChar 
		
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2022-09-14-01.DataFlowViewsReturnAttributesWhenNoObsComponents.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
				
	END CATCH

	DROP TABLE #Temp_DSD_Components

END

--update transaction info
UPDATE  [management].[DSD_TRANSACTION] SET [EXECUTION_END] = GETDATE(), [SUCCESSFUL] = 1
WHERE [TRANSACTION_ID] = @Transaction_ID