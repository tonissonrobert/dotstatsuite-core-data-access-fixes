DECLARE @SchemaName NVARCHAR(128) = 'data';
DECLARE @TableNamePattern NVARCHAR(128) = 'FACT%';
DECLARE @TableName NVARCHAR(128);
DECLARE @ColumnName NVARCHAR(128) = 'VALUE'
DECLARE @DataType NVARCHAR(128) = 'varchar'
DECLARE @OldColumnLength1 INT = 6; 
DECLARE @NewColumnLength1 INT = 13;
DECLARE @OldColumnLength2 INT = 10;
DECLARE @NewColumnLength2 INT = 22;
DECLARE @OldColumnLength INT;
DECLARE @NewColumnLength INT;

DECLARE @TransactionId INT
DECLARE @LogLevel NVARCHAR(50)
DECLARE @Logger NVARCHAR(255) = 'MSSQL SERVER:'+'2023-10-31-01.AlterValueColumnsWithVarcharType.sql'
DECLARE @Application NVARCHAR(255) = 'dotstatsuite-dbup'
DECLARE @Message NVARCHAR(MAX)
DECLARE @Exception NVARCHAR(MAX)
DECLARE @ColumnCount INT
DECLARE @FailedColumnCount INT

-- Generate the next value for [management].[TRANSFER_SEQUENCE] as the Transaction ID
SELECT @TransactionId = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE]

DECLARE tableCursor CURSOR FOR
SELECT TABLE_NAME, CHARACTER_MAXIMUM_LENGTH AS OLD_LENGTH, CASE CHARACTER_MAXIMUM_LENGTH WHEN @OldColumnLength1 THEN @NewColumnLength1 WHEN @OldColumnLength2 THEN @NewColumnLength2 ELSE CHARACTER_MAXIMUM_LENGTH END AS NEW_LENGTH
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = @SchemaName
    AND TABLE_NAME LIKE @TableNamePattern
    AND COLUMN_NAME = @ColumnName
    AND DATA_TYPE = @DataType
    AND CHARACTER_MAXIMUM_LENGTH IN (@OldColumnLength1, @OldColumnLength2);

INSERT 
  INTO [management].[DSD_TRANSACTION]
       ([TRANSACTION_ID], [ART_ID], [TABLE_VERSION], [EXECUTION_START], [EXECUTION_END], [SUCCESSFUL], [ARTEFACT_FULL_ID], [USER_EMAIL])
VALUES (@TransactionId, -1, 'X', GETDATE(), GETDATE(), 0, 'N/A', NULL)

OPEN tableCursor
FETCH NEXT FROM tableCursor INTO @TableName, @OldColumnLength, @NewColumnLength

SET @ColumnCount = 0
SET @FailedColumnCount = 0

WHILE @@FETCH_STATUS = 0
BEGIN
    BEGIN TRY
        DECLARE @AlterStatement NVARCHAR(MAX)
        SET @AlterStatement = N'ALTER TABLE [' + @SchemaName + N'].[' + @TableName + N'] ALTER COLUMN [' + @ColumnName + N'] ' + @DataType + N'(' + CAST(@NewColumnLength AS NVARCHAR(10)) + N')' 
        EXEC(@AlterStatement)

        SET @LogLevel = 'INFO'
        SET @Message = N'Column [' + @SchemaName + N'].[' + @TableName + N'].[' + @ColumnName + N'] data type changed successfully.'
        SET @Exception = NULL

        SET @ColumnCount = @ColumnCount + 1
    END TRY
    BEGIN CATCH
        SET @LogLevel = 'ERROR'
        SET @Message = N'Error altering column [' + @SchemaName + N'].[' + @TableName + N'].[' + @ColumnName + N'] data type. ' + ERROR_MESSAGE()
        SET @Exception = ERROR_MESSAGE()

        SET @FailedColumnCount = @FailedColumnCount + 1
    END CATCH

    -- Insert log entry into [management].[LOGS] table
    INSERT INTO [management].[LOGS] (
        [TRANSACTION_ID], [DATE], [LEVEL], [SERVER], [LOGGER], [MESSAGE], [EXCEPTION], [APPLICATION]
    )
    VALUES (
        @TransactionId, GETDATE(), @LogLevel, @@SERVERNAME, @Logger, @Message, @Exception, @Application
    )

    FETCH NEXT FROM tableCursor INTO @TableName, @OldColumnLength, @NewColumnLength
END

CLOSE tableCursor
DEALLOCATE tableCursor

-- Log summary message
IF @ColumnCount = 0 AND @FailedColumnCount = 0
    SET @Message = N'No columns need to be altered.'
ELSE 
  IF @ColumnCount > 0
    SET @Message = CAST(@ColumnCount AS NVARCHAR(10)) + N' columns altered successfully.'
  ELSE
    SET @Message = @Message + ' ' + CAST(@FailedColumnCount AS NVARCHAR(10)) + N' columns failed to be altered.'

-- Insert summary log entry into [management].[LOGS] table
INSERT INTO [management].[LOGS] (
    [TRANSACTION_ID], [DATE], [LEVEL], [SERVER], [LOGGER], [MESSAGE], [APPLICATION]
)
VALUES (
    @TransactionId, GETDATE(), 'INFO', @@SERVERNAME, @Logger, @Message, @Application
)

UPDATE [management].[DSD_TRANSACTION] 
   SET [EXECUTION_END] = GETDATE(), 
       [SUCCESSFUL] = 1,
       [STATUS] = 'Completed'
 WHERE [TRANSACTION_ID] = @TransactionId
