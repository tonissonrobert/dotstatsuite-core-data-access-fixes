﻿--Set QUEUED_DATETIME value to EXECUTION_START, for the entries that already existed before the addition of the field QUEUED_DATETIME
UPDATE [management].[DSD_TRANSACTION]
SET [QUEUED_DATETIME] = [EXECUTION_START]
WHERE [QUEUED_DATETIME] > [EXECUTION_START]