SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL ON
GO
  
DECLARE 	
	@DSD_ID int,
	@COMP_ID int,	
	@COMP_CODE varchar(50),	
	@COMP_TYPE varchar(50),
	@CL_ID int,	
	@COMP_LEVEL varchar(15),
	@COMP_STATUS varchar(11),

	@COMP_ID_STR varchar(20),

	@sql NVARCHAR(MAX),	
	@sql_migration NVARCHAR(MAX),
	@sql_migration_select NVARCHAR(max),
	
	@sql_view NVARCHAR(MAX),
	@sql_view_select NVARCHAR(max),
	@sql_view_name varchar(100),

	@old_attr_table varchar(100),
	@new_attr_table varchar(100),
	@component_alias varchar(100),
	@table_version char(1),
	
	@HasTimeDim as bit = 0,
    @NewLineChar AS CHAR(2) =  CHAR(10);

DECLARE 
	@VERSIONS TABLE ([version] char(1));
	
DECLARE
	@DSD_IDS TABLE(ART_ID int PRIMARY KEY);

INSERT into @DSD_IDS 
	SELECT ART_ID FROM [management].[ARTEFACT]
	WHERE [type] = 'DSD' ORDER BY ART_ID
	
WHILE (Select Count(*) FROM @DSD_IDS) > 0
BEGIN

    SELECT TOP 1 @DSD_ID = ART_ID FROM @DSD_IDS
	DELETE @DSD_IDS WHERE ART_ID = @DSD_ID
	
--------------------------------------------------------
	SET @old_attr_table = '[data].ATTR_' + CAST(@DSD_ID AS VARCHAR) + '_{V}_DIMGROUP'
	SET @new_attr_table = 'ATTR_' + CAST(@DSD_ID AS VARCHAR) + '_{V}'	
--------------------------------------------------------

	IF (Object_ID(REPLACE(@old_attr_table,'{V}','A')) IS NULL)
	BEGIN
		CONTINUE;
	END	

	print 'Migrating DSD: ' + CAST(@DSD_ID AS VARCHAR)

	SET @sql_migration = '';
	SET @sql_migration_select = null;
	SET @sql_view = '';
	SET @sql_view_select = null;

--------------------------------------------------------
	DELETE @VERSIONS
	INSERT into @VERSIONS SELECT 'A' union all SELECT 'B'
--------------------------------------------------------

	IF EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DIM_TIME' AND Object_ID = Object_ID(N'[data].[FACT_'+CAST(@DSD_ID AS VARCHAR)+'_A]'))
	BEGIN
		SELECT @HasTimeDim=1
	END	
----------------------------START BUILD SELECT STATEMENT---------------------------

	DECLARE cursor_component CURSOR
	FOR  SELECT c.COMP_ID,c.ID,c.[TYPE],c.CL_ID,c.ATT_ASS_LEVEL, c.ATT_STATUS
    FROM [management].[COMPONENT] c 
    WHERE c.DSD_ID=@DSD_ID

	OPEN cursor_component;
	FETCH NEXT FROM cursor_component INTO @COMP_ID,@COMP_CODE,@COMP_TYPE,@CL_ID,@COMP_LEVEL,@COMP_STATUS
	
	WHILE @@FETCH_STATUS = 0
		BEGIN
			
			DECLARE @target_table varchar(10) = 'filt'
			SET @component_alias = '[' + @COMP_CODE +']'
			SET @COMP_ID_STR = '[DIM_'+CAST(@COMP_ID AS VARCHAR) + ']'
			

			
			IF(@COMP_TYPE = 'Attribute')
			BEGIN

				SET @COMP_ID_STR = '[COMP_'+CAST(@COMP_ID AS VARCHAR) + ']'

				-- Observation attribute ----------------------------
				IF(@COMP_LEVEL = 'Observation')
				BEGIN
					SET @target_table = 'fact'
				END
				-- Group attribute ----------------------------
				ELSE IF(@COMP_LEVEL = 'DimensionGroup' OR @COMP_LEVEL = 'Group')
				BEGIN

					SET @target_table = 'attr'
					set @sql_migration_select = CONCAT(@sql_migration_select + ',', @component_alias + '.' + @COMP_ID_STR)

					SET @sql_migration 
						+= CASE WHEN @COMP_STATUS='Mandatory' THEN 'INNER' ELSE 'LEFT' END
						+ ' JOIN ' + @old_attr_table + ' AS ' + @component_alias + ' ON'

					set @sql_migration += STUFF(( 
						SELECT 'AND ' + @component_alias + '.[DIM_' + CAST(c.COMP_ID AS VARCHAR) + ']' + CASE 
							WHEN a.DIM_ID is NULL then ' IS NULL '
							ELSE ' = filt.[DIM_' + CAST(c.COMP_ID AS VARCHAR) + '] '
						END
						FROM [management].[COMPONENT] c  
						LEFT JOIN [management].[ATTR_DIM_SET] a ON c.COMP_ID = a.DIM_ID AND a.ATTR_ID = @COMP_ID 
						WHERE c.DSD_ID=@DSD_ID AND c.[TYPE] = 'Dimension' 
						FOR XML PATH('')
					), 1,3, '')
					+ @NewLineChar
				END
				-- Group attribute ----------------------------
				ELSE
				BEGIN
					Goto SKIP_COMPONENT
				END
			END

			----------------------------------

			IF(@CL_ID IS NOT NULL)
			BEGIN
				set @sql_view_select = CONCAT(@sql_view_select + ',', @component_alias + '.ID ' + @component_alias)
				
				SET @sql_view 
					+= CASE WHEN @COMP_TYPE = 'Dimension' OR @COMP_STATUS='Mandatory' THEN 'INNER' ELSE 'LEFT' END
					+ ' JOIN [management].[CL_'+CAST(@CL_ID AS VARCHAR)+'] AS '+ @component_alias 
					+' ON ' + @target_table + '.' + @COMP_ID_STR + ' = ' + @component_alias + '.[ITEM_ID]' 
					+ @NewLineChar
			END
				ELSE
			BEGIN
				set @sql_view_select = CONCAT(@sql_view_select + ',', @target_table + '.' + @COMP_ID_STR + ' ' + @component_alias)
			END
			----------------------------------

			SKIP_COMPONENT:
				FETCH NEXT FROM cursor_component INTO @COMP_ID,@COMP_CODE,@COMP_TYPE,@CL_ID,@COMP_LEVEL,@COMP_STATUS
		END;

	CLOSE cursor_component;
	DEALLOCATE cursor_component;

	SET @sql_migration = 'SELECT filt.SID,' + @sql_migration_select + @NewLineChar
		+ 'INTO [data].' + @new_attr_table + @NewLineChar
		+ 'FROM [data].[FILT_'+CAST(@DSD_ID AS VARCHAR)+'] filt' + @NewLineChar
		+ @sql_migration
		+ 'WHERE EXISTS (SELECT 1 FROM [data].[FACT_'+CAST(@DSD_ID AS VARCHAR)+'_{V}] WHERE [SID]=filt.[SID])' + @NewLineChar
		+ 'AND (' + REPLACE(@sql_migration_select, ',',' IS NOT NULL OR ') + '  IS NOT NULL)' + @NewLineChar

	--------------------------------------------
	IF (@HasTimeDim = 1)
	BEGIN
		SET @sql_view += 'INNER JOIN [management].[CL_TIME] AS T ON T.[ITEM_ID]=fact.[DIM_TIME]' + @NewLineChar
		SET @sql_view_select = CONCAT(@sql_view_select + ',', '[T].[ID] AS [TIME_PERIOD]')
	END

	SET @sql_view_select = CONCAT(@sql_view_select + ',', '[fact].[VALUE] AS [OBS_VALUE]')

	SET @sql_view = 'SELECT ' + @sql_view_select + @NewLineChar 
		+ 'FROM [data].[FILT_'+CAST(@DSD_ID AS VARCHAR)+'] filt' + @NewLineChar
		+ 'INNER JOIN [data].[FACT_'+CAST(@DSD_ID AS VARCHAR)+'_{V}] fact ON filt.[SID] = fact.[SID]'+ @NewLineChar
		+ 'LEFT JOIN [data].' + @new_attr_table + ' attr ON filt.[SID] = attr.[SID]' + @NewLineChar
		+ @sql_view
	--------------------------------------------

	BEGIN TRY
		BEGIN TRAN
		WHILE (Select Count(*) FROM @VERSIONS) > 0
		BEGIN
			
			SELECT top 1 @table_version = [version] from @VERSIONS
			DELETE FROM @VERSIONS where [version]=@table_version

			-- migrate group attributes
			SELECT @sql = REPLACE(@sql_migration, '{V}', @table_version)
			--print @sql
			exec sp_executesql @sql

			-- create index
			SELECT @sql = 'ALTER TABLE [data].' + @new_attr_table + ' ADD CONSTRAINT ' + @new_attr_table + '_PK PRIMARY KEY ([SID])'
			SELECT @sql = REPLACE(@sql, '{V}', @table_version)
			--print @sql
			exec sp_executesql @sql

			-- drop old attribute table
			SELECT @sql = 'DROP TABLE ' + REPLACE(@old_attr_table, '{V}', @table_version)
			--print @sql
			exec sp_executesql @sql

			set @sql_view_name = '[data].VI_CurrentDataDsd_'+ CAST( @DSD_ID AS VARCHAR) + '_' + @table_version

			-- drop existing view	
			IF OBJECT_ID(@sql_view_name, 'V') IS NOT NULL
			BEGIN
				EXEC  ('DROP VIEW '+ @sql_view_name)
			END
		
			-- create new view
			SELECT @sql = 'CREATE VIEW ' + @sql_view_name +' AS '+ REPLACE(@sql_view,'{V}', @table_version);
			--print @sql
			exec sp_executesql @sql
		END
		COMMIT TRAN
	END TRY  
	BEGIN CATCH  
		
		ROLLBACK TRAN

		DECLARE 
			@DSD nvarchar(100),
			@msg nvarchar(1000),
			@Transaction_ID int

		SELECT @DSD  = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+'.'+ CASE WHEN [VERSION_3]IS NULL THEN '0' ELSE CAST([VERSION_3] AS VARCHAR) END +')'
		FROM [management].[ARTEFACT] 
		WHERE [ART_ID] = @DSD_ID AND [TYPE] = 'DSD'

		--Create new transaction for DSD
		SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		INSERT INTO [management].[DSD_TRANSACTION] 
		([TRANSACTION_ID],[ART_ID],[DSD_TARGET_VERSION],[EXECUTION_START],[EXECUTION_END],[SUCCESSFUL],[ARTEFACT_FULL_ID],[USER_EMAIL])
		VALUES (@Transaction_ID, @DSD_ID, 'X',GETDATE(),GETDATE(),0, @DSD,NULL)
		
		SELECT @msg= 'The following error was found while trying to create the DSD Views for the DSD ['+ @DSD +'] (The creation of the view will be skiped):'+ ' ErrorNumber:'+CAST(ERROR_NUMBER() AS VARCHAR)+' ErrorMessage:'+ERROR_MESSAGE()+@NewLineChar 
	
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2020-11-20-01.MigrateAttributes.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
		
		--update transaction info
		UPDATE  [management].[DSD_TRANSACTION] 
		SET [EXECUTION_END]= GETDATE(), [SUCCESSFUL] = 1,TIMED_OUT=0
		WHERE [TRANSACTION_ID]=@Transaction_ID

	END CATCH
END
