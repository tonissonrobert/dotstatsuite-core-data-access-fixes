﻿SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL ON
GO
  
DECLARE 	
	@DSD_ID int,
	@has_time_dim bit,
	@time_dim_part NVARCHAR(MAX),
	@sql_migration NVARCHAR(MAX),	
	@table_version char(1),	
    @NewLineChar AS CHAR(2) =  CHAR(10);

DECLARE 
	@VERSIONS TABLE ([version] char(1));
	
DECLARE
	@DSD_IDS TABLE(ART_ID int PRIMARY KEY);

INSERT into @DSD_IDS 
	SELECT ART_ID FROM [management].[ARTEFACT]
	WHERE [type] = 'DSD' ORDER BY ART_ID
	
WHILE (Select Count(*) FROM @DSD_IDS) > 0
BEGIN

    SELECT TOP 1 @DSD_ID = ART_ID FROM @DSD_IDS
	DELETE @DSD_IDS WHERE ART_ID = @DSD_ID
	
	print 'Migrating DSD: ' + CAST(@DSD_ID AS VARCHAR)

--------------------------------------------------------
	DELETE @VERSIONS
	INSERT into @VERSIONS SELECT 'A' union all SELECT 'B'
--------------------------------------------------------

	BEGIN TRY
		BEGIN TRAN
		WHILE (Select Count(*) FROM @VERSIONS) > 0
		BEGIN
			-- select next version
			SELECT top 1 @table_version = [version] from @VERSIONS
			DELETE FROM @VERSIONS where [version]=@table_version

			IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N'[data].[FACT_'+CAST(@DSD_ID AS VARCHAR)+'_' + @table_version + ']'))
			BEGIN
				CONTINUE
			END
			
			SELECT @time_dim_part = '';
			SELECT @has_time_dim = 0;
			IF EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PERIOD_SDMX' AND Object_ID = Object_ID(N'[data].[FACT_'+CAST(@DSD_ID AS VARCHAR)+'_A]'))
			BEGIN
				SELECT @has_time_dim = 1;
				SELECT @time_dim_part = ', PERIOD_START ASC, PERIOD_END DESC';
			END

			-- drop pk
			set @sql_migration = 'ALTER TABLE data.FACT_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version + ' DROP CONSTRAINT IF EXISTS PK_DSD_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version;
			exec sp_executesql @sql_migration;

			-- add pk
			set @sql_migration = 'ALTER TABLE data.FACT_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version + ' ADD CONSTRAINT PK_DSD_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version + ' PRIMARY KEY (SID ASC' + @time_dim_part + ')';
			exec sp_executesql @sql_migration;
			
			-- drop old time index
			set @sql_migration = 'DROP INDEX IF EXISTS I_TIME_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version + ' ON data.FACT_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version;
			exec sp_executesql @sql_migration;
			
			-- drop time index if already exists
			set @sql_migration = 'DROP INDEX IF EXISTS NCCI_TIME_DIM_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version + ' ON data.FACT_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version;
			exec sp_executesql @sql_migration;

			IF @has_time_dim = 1
			BEGIN
				-- add time index
				set @sql_migration = 'CREATE NONCLUSTERED COLUMNSTORE INDEX NCCI_TIME_DIM_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version + ' ON data.FACT_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version + '(PERIOD_START,PERIOD_END)';
				exec sp_executesql @sql_migration;
			END
				
		END
		COMMIT TRAN
	END TRY  
	BEGIN CATCH  		
		
		ROLLBACK TRAN

		DECLARE 
			@DSD nvarchar(100),
			@msg nvarchar(1000),
			@Transaction_ID int

		SELECT @DSD  = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+'.'+ CASE WHEN [VERSION_3]IS NULL THEN '0' ELSE CAST([VERSION_3] AS VARCHAR) END +')'
		FROM [management].[ARTEFACT] 
		WHERE [ART_ID] = @DSD_ID AND [TYPE] = 'DSD'
	
		--Create new transaction for DSD
		SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		
		INSERT INTO [management].[DSD_TRANSACTION] 
		([TRANSACTION_ID],[ART_ID],[DSD_TARGET_VERSION],[EXECUTION_START],[EXECUTION_END],[SUCCESSFUL],[ARTEFACT_FULL_ID],[USER_EMAIL])
		VALUES (@Transaction_ID, @DSD_ID, 'X',GETDATE(),GETDATE(),0, @DSD,null)
		
		SELECT @msg= 'The following error was found while trying to modify the indexes of DSD ['+ @DSD +'] (The modification of the indexes will be skipped. Please Delete the dsd, run the cleanup function and re-upload the data.):'+ ' ErrorNumber:'+CAST(ERROR_NUMBER() AS VARCHAR)+' ErrorMessage:'+ERROR_MESSAGE()+@NewLineChar 
		
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2021-09-07-01.PatchRecreateFactTableIndexes.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
		
		--update transaction info
		UPDATE  [management].[DSD_TRANSACTION] 
		SET [EXECUTION_END]= GETDATE(), [SUCCESSFUL] = 1,TIMED_OUT=0
		WHERE [TRANSACTION_ID]=@Transaction_ID
		
	END CATCH
END
