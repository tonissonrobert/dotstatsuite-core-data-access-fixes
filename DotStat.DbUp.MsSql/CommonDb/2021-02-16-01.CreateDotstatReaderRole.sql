-- Create role if it does not exist yet
IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE TYPE = 'R' AND name = 'DotStatReader')
BEGIN
   CREATE ROLE [DotStatReader];
END

-- Grant SELECT permission on authorization table
GRANT SELECT ON [dbo].[AUTHORIZATIONRULES] TO [DotStatReader]

-- Grant SELECT permission on db version
GRANT SELECT ON [dbo].DB_VERSION TO [DotStatReader]
