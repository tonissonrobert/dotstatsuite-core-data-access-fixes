-- Create role if it does not exist yet
IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE TYPE = 'R' AND name = 'DotStatWriter')
BEGIN
   CREATE ROLE [DotStatWriter]
END

-- Grant SELECT permission on authorization table
GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[AUTHORIZATIONRULES] TO [DotStatWriter]

-- Grant SELECT permission on db version
GRANT SELECT ON [dbo].DB_VERSION TO [DotStatWriter]

-- If user already exists in db, then remove roles and permissions added by previous version of DbUp and add login to DotStatWriter role
IF EXISTS (SELECT 1 FROM sys.database_principals WHERE TYPE = 'S' AND name = '$loginName$')
BEGIN    
    ALTER ROLE [DotStatWriter] ADD MEMBER [$loginName$];

	-- Remove user from datareader and datawriter roles in case it was added to them by a previous version of DbUp
	ALTER ROLE [db_datareader] DROP MEMBER [$loginName$];
	ALTER ROLE [db_datawriter] DROP MEMBER [$loginName$];

	-- Revoke direct permissions in case they were granted by previous version of DbUp
	REVOKE SELECT, INSERT, UPDATE, DELETE ON [dbo].[AUTHORIZATIONRULES] FROM [$loginName$];
	REVOKE SELECT ON [dbo].DB_VERSION FROM [$loginName$];
END
