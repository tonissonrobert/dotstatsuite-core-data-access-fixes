﻿-- <db_user_access_option> of ALTER DATABASE command is not supported on Azure SQL Managed Instance
IF SERVERPROPERTY('EngineEdition') <> 8 
BEGIN
   EXECUTE sp_executesql N'ALTER DATABASE [$dbName$] SET RESTRICTED_USER WITH ROLLBACK IMMEDIATE;';
END
