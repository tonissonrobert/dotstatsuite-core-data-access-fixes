﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Db.Exception
{
    [ExcludeFromCodeCoverage]
    public class TransactionTimeOutException : DotStatException
    {
        public TransactionTimeOutException()
        {
        }

        public TransactionTimeOutException(string message) : base(message)
        {
        }

        public TransactionTimeOutException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
