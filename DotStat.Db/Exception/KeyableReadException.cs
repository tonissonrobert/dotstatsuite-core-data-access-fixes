﻿using System.Diagnostics.CodeAnalysis;
using DotStat.DB.Exception;
using DotStat.Db.Validation;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Db.Exception
{
    [ExcludeFromCodeCoverage]
    public class KeyableReadException : SdmxValidationException
    {
        public KeyableReadException(ValidationErrorType errorType, IKeyable keyable, IKeyValue keyValue, params string[] argument)
            : base(new KeyableError()
            {
                Type = errorType,
                Coordinate = keyable?.GetFullKey(true),
                Value = keyValue == null ? null : $"{keyValue.Concept}:{keyValue.Code}",
                Argument = argument
            })
        {

        }
    }
}