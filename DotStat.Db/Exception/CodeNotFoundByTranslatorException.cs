﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Db.Exception
{
    [ExcludeFromCodeCoverage]
    class CodeNotFoundByTranslatorException : DotStatException
    {
        public CodeNotFoundByTranslatorException()
        {
        }

        public CodeNotFoundByTranslatorException(string message) : base(message)
        {
        }

        public CodeNotFoundByTranslatorException(string message, System.Exception innerException) : base(message, innerException)
        {
        }

    }
}
