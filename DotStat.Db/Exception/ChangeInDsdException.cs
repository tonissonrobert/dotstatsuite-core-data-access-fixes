﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Db.Exception
{
    [ExcludeFromCodeCoverage]
    class CleanupOfTransactionFailedException: DotStatException
    {
        public CleanupOfTransactionFailedException( )
        {
        }

        public CleanupOfTransactionFailedException( string message) : base(message)
        {
        }

        public CleanupOfTransactionFailedException( string message, System.Exception innerException) : base(message, innerException)
        {
        }

    }
}
