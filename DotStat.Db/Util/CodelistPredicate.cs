﻿using System.Collections.Generic;
using System.Linq;

namespace DotStat.Db.Util
{
    public class CodelistPredicate : Predicate
    {
        private string DimColumn
        {
            get;
        }
        private List<string> Values
        {
            get;
        }
        private NullTreatment NullTreatment
        {
            get;
        }
        private bool UseExternalValues
        {
            get;
        }

        public CodelistPredicate(string dimColumn, IEnumerable<string> values, NullTreatment nullTreatment = NullTreatment.ExcludeNulls, bool useExternalValues = true)
        {
            DimColumn = dimColumn;
            Values = new List<string>(values);
            NullTreatment = nullTreatment;
            UseExternalValues = useExternalValues;
        }

        public override string ToString()
        {
            return ToString(NullTreatment);
        }

        public override string ToString(NullTreatment nullTreatment)
        {
            var dimensionClause = string.Join(" OR ", Values.Select(id => DimColumn + "=" + $"{id}"));
            var nullClause = "";
            if (nullTreatment == NullTreatment.IncludeNulls) {
                nullClause = (string.IsNullOrWhiteSpace(dimensionClause) ? "" : " OR ") + DimColumn + " IS NULL";
            }
            else if (nullTreatment == NullTreatment.IncludeNullsAndSwitchOff)
            {
                var switchedOffValue = UseExternalValues ? $"'{DbExtensions.DimensionSwitchedOff}'" : $"{DbExtensions.DimensionSwitchedOffDbValue}";
                nullClause = (string.IsNullOrWhiteSpace(dimensionClause) ? "" : " OR ") + $"{DimColumn} IS NULL OR {DimColumn} = {switchedOffValue}";
            }

            return !string.IsNullOrWhiteSpace(dimensionClause) || !string.IsNullOrWhiteSpace(nullClause)
                ? $"({dimensionClause}{nullClause})"
                : string.Empty;
        }
    }
}