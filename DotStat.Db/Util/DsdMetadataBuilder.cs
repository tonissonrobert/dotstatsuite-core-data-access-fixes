﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.Db;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.DB.Util
{
    public class DimMeta
    {
        public int Index { get; }
        public Dimension Dimension { get; }

        public DimMeta(int index, Dimension dimension)
        {
            Index = index;
            Dimension = dimension;
        }
    }

    public class AttrMeta
    {
        public int Index { get; }
        public Attribute Attribute { get; }

        public AttrMeta(int index, Attribute attribute)
        {
            Index = index;
            Attribute = attribute;
        }
    }
    public class MetaAttrMeta
    {
        public int Index { get; }
        public MetadataAttribute MetadataAttribute { get; }

        public MetaAttrMeta(int index, MetadataAttribute metadataAttribute)
        {
            Index = index;
            MetadataAttribute = metadataAttribute;
        }
    }

    public class DsdMetadataBuilder
    {
        public readonly Dsd Dsd;
        public readonly List<Dimension> NonTimeDimensions;
        public readonly int DimensionCount;
        public Dictionary<string, List<string>> DimensionsReferenceGroups { get; }

        public readonly bool HasTime;
        public readonly Dimension TimeDim;
        public readonly bool HasReportingYearStartDayAttr;
        private readonly Dictionary<string, DimMeta> _dimCodeToIndexMap;
        private readonly Dictionary<string, AttrMeta> _attrCodeToIndexMap;
        private readonly Dictionary<string, MetaAttrMeta> _metaAttrCodeToIndexMap;

        public DsdMetadataBuilder(Dsd dsd, ReportedComponents reportedComponents)
        {
            _dimCodeToIndexMap = new Dictionary<string, DimMeta>();
            _attrCodeToIndexMap = new Dictionary<string, AttrMeta>();
            _metaAttrCodeToIndexMap = new Dictionary<string, MetaAttrMeta>();

            Dsd = dsd;
            NonTimeDimensions = Dsd.Dimensions.Where(d => !d.Base.TimeDimension).ToList();

            HasTime = false;
            DimensionCount = 0;

            if (reportedComponents.TimeDimension is not null)
            {
                DimensionCount += 2;
                TimeDim = reportedComponents.TimeDimension;
                HasTime = true;
            }

            foreach (var dim in dsd.Dimensions.OrderTimeFirst())
            {
                if (reportedComponents.Dimensions.Any(r => r.Code.Equals(dim.Code, StringComparison.InvariantCultureIgnoreCase)))
                {
                    _dimCodeToIndexMap.Add(dim.Code, new DimMeta(DimensionCount++, dim));
                }
            }

            var attributeCount = 0;
            foreach (var attr in dsd.Attributes.OrderBy(a => a.DbId))
            {
                if (attr.Code == AttributeObject.Repyearstart)
                {
                    HasReportingYearStartDayAttr = true;
                }

                _attrCodeToIndexMap.Add(attr.Code, new AttrMeta(
                    attributeCount++,
                    attr
                ));
            }

            var metadataAttributeCount = 0;
            if (Dsd.Msd != null)
            {
                foreach (var metaAttr in Dsd.Msd.MetadataAttributes.OrderBy(a => a.DbId))
                {
                    if (metaAttr.HierarchicalId == AttributeObject.Repyearstart)
                    {
                        HasReportingYearStartDayAttr = true;
                    }

                    _metaAttrCodeToIndexMap.Add(metaAttr.HierarchicalId, new MetaAttrMeta(
                        metadataAttributeCount++,
                        metaAttr
                    ));
                }
            }

            DimensionsReferenceGroups = new Dictionary<string, List<string>>();
            if(reportedComponents.IsPrimaryMeasureReported)
                DimensionsReferenceGroups.Add(Dsd.Base.PrimaryMeasure.Id, Dsd.Dimensions.Select(d => d.Code).ToList());

            foreach (var attribute in Dsd.Attributes)
            {
                switch (attribute.Base.AttachmentLevel)
                {
                    //Observation level
                    case AttributeAttachmentLevel.Observation:
                    {
                        var dimensionsReferenced = Dsd.Dimensions.Select(d=>d.Code).ToList();
                        DimensionsReferenceGroups.Add(attribute.Code, dimensionsReferenced);
                        break;
                    }
                    //DataSet Level
                    case AttributeAttachmentLevel.DataSet 
                        or AttributeAttachmentLevel.Null:
                        DimensionsReferenceGroups.Add(attribute.Code, new List<string>());
                        break;
                    //All other levels
                    default:
                    {
                        var dimensionsCodeReferenced = attribute.Base.DimensionReferences;
                        var dimensionsReferenced = Dsd.Dimensions
                            .Where(dim => dimensionsCodeReferenced.Contains(dim.Code))
                            .Select(d => d.Code).ToList();

                        if (dimensionsReferenced.Count <= 0)
                        {
                            var groupDimRefs = Dsd.Base.Groups.Where(g =>
                                    g.Id.Equals(attribute.Base.AttachmentGroup,
                                        StringComparison.InvariantCultureIgnoreCase)).Select(g => g.DimensionRefs)
                                .FirstOrDefault();

                            dimensionsReferenced = groupDimRefs is null ? Dsd.Dimensions.Select(d => d.Code).ToList()
                                : Dsd.Dimensions
                                    .Where(dim => groupDimRefs.Contains(dim.Code)).Select(d => d.Code).ToList();
                        }

                        DimensionsReferenceGroups.Add(attribute.Code, dimensionsReferenced);
                        break;
                    }
                }
            }
        }

        public DimMeta this[string code] => _dimCodeToIndexMap[code];
        public DimMeta Dim(string code) => _dimCodeToIndexMap[code];
        public AttrMeta Attr(string code) => _attrCodeToIndexMap[code];
        public MetaAttrMeta MetaAttr(string code) => _metaAttrCodeToIndexMap[code];
        public bool IsValidDim(string code) => _dimCodeToIndexMap.ContainsKey(code);
        public bool IsValidAttr(string code) => _attrCodeToIndexMap.ContainsKey(code);
        public bool IsValidMetaAttr(string code) => _metaAttrCodeToIndexMap.ContainsKey(code);
        public IEnumerable<DimMeta> Dims => _dimCodeToIndexMap.Values.OrderBy(x=>x.Index);
        public IEnumerable<AttrMeta> Attrs => _attrCodeToIndexMap.Values.OrderBy(x=>x.Index);
        public IEnumerable<MetaAttrMeta> MetaAttrs => _metaAttrCodeToIndexMap.Values.OrderBy(x => x.Index);
    }
}