﻿using System.Collections.Generic;
using System.Linq;

namespace DotStat.Db.Util
{
    public abstract class AttributePredicate : Predicate
    {
        public string AttributeColumn
        {
            get;
        }
        protected List<int> Values
        {
            get;
        }
        protected NullTreatment NullTreatment
        {
            get;
        }

        protected bool IsObservationAttribute
        {
            get;
        }

        protected AttributePredicate(string attrColumn, IEnumerable<int> values, bool isObservationAttribute, NullTreatment nullTreatment = NullTreatment.ExcludeNulls)
        {
            IsObservationAttribute = isObservationAttribute;
            AttributeColumn = attrColumn;
            Values = new List<int>(values);
            NullTreatment = nullTreatment;
        }

        public override string ToString()
        {
            return ToString(NullTreatment);
        }

        public override string ToString(NullTreatment nullTreatment)
        {
            var attributeClause = string.Join(" OR ", Values.Select(id => AttributeColumn + "=" + id));
            var nullClause = nullTreatment == NullTreatment.IncludeNulls
                ? (string.IsNullOrWhiteSpace(attributeClause) ? "" : " OR ") + AttributeColumn + " IS NULL"
                : "";
            return $"({attributeClause}{nullClause})";
        }
    }

    public class ObservationAttributePredicate : AttributePredicate
    {
        public ObservationAttributePredicate(string attrColumn, IEnumerable<int> values, NullTreatment nullTreatment = NullTreatment.ExcludeNulls) : base(attrColumn, values, true, nullTreatment)
        {
        }
    }

    public class DimGroupAttributePredicate : AttributePredicate
    {
        public DimGroupAttributePredicate(string attrColumn, IEnumerable<int> values, NullTreatment nullTreatment = NullTreatment.IncludeNulls) : base(attrColumn, values, false, nullTreatment)
        {
        }
    }
}