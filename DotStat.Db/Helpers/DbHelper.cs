﻿using System.Data.Common;

namespace DotStat.Db.Helpers
{
    public static class DbHelper
    {
        public const string DbConnectionAppName = ".Stat V8";

        public static T? GetNullableValue<T>(this DbDataReader dataReader, string columnName) where T : struct
        {
            var columnIndex = dataReader.GetOrdinal(columnName);
            return GetNullableValue<T>(dataReader, columnIndex);
        }

        public static T? GetNullableValue<T>(this DbDataReader dataReader, int columnIndex) where T : struct
        {
            if (!dataReader.IsDBNull(columnIndex))
            {
                return (T)dataReader[columnIndex];
            }

            return null;
        }

        public static string GetNullableString(this DbDataReader dataReader, string columnName)
        {
            var columnIndex = dataReader.GetOrdinal(columnName);
            return GetNullableString(dataReader, columnIndex);
        }

        public static string GetNullableString(this DbDataReader dataReader, int columnIndex)
        {
            if (!dataReader.IsDBNull(columnIndex))
            {
                return dataReader.GetString(columnIndex);
            }

            return null;
        }
    }
}
