﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.DB;
using DotStat.Domain;

namespace DotStat.Db.Validation
{
    public interface IDatasetAttributeDatabaseValidator
    {
        Task<bool> Validate(IDotStatDb dotStatDataDb, Dataflow dataflow, DbTableVersion tableVersion, List<Attribute> reportedAttributes, IList<DataSetAttributeRow> dataSetAttributeRows, int? maxErrorCount, bool fullValidation, CancellationToken cancellationToken);

        List<IValidationError> GetErrors();

        string GetErrorsMessage();

    }
}
