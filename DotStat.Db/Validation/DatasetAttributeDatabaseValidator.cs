﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.DB;
using DotStat.Db.Repository;
using DotStat.Db.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.Db.Validation
{
    
    public abstract class DatasetAttributeDatabaseValidator : IDatasetAttributeDatabaseValidator
    {
        private IList<DataSetAttributeRow> _dataSetAttributeRows;
        private int _maxErrorCount;
        protected Dataflow Dataflow;
        private List<IValidationError> _errors;
        private bool _isValid;
        protected List<Attribute> MandatoryDatasetAttributes;
        
        protected IGeneralConfiguration GeneralConfiguration;

        protected DatasetAttributeDatabaseValidator(IGeneralConfiguration generalConfiguration)
        {
            GeneralConfiguration =
                generalConfiguration ?? throw new ArgumentNullException(nameof(generalConfiguration));

            _maxErrorCount = GeneralConfiguration.MaxTransferErrorAmount;

            _errors = new List<IValidationError>();
        }

        public async Task<bool> Validate(IDotStatDb dotStatDataDb, Dataflow dataflow, DbTableVersion tableVersion,
            List<Attribute> reportedAttributes, IList<DataSetAttributeRow> dataSetAttributeRows, int? maxErrorCount,
            bool fullValidation, CancellationToken cancellationToken)
        {
            if (dotStatDataDb == null) throw new ArgumentNullException(nameof(dotStatDataDb));

            if (!fullValidation)
                return true;

            Dataflow = dataflow ?? throw new ArgumentNullException(nameof(dataflow));

            _dataSetAttributeRows = (dataSetAttributeRows ?? new List<DataSetAttributeRow>())
                .Where(a => a.Action is StagingRowActionEnum.Merge).ToList();
            
            _maxErrorCount = maxErrorCount ?? GeneralConfiguration.MaxTransferErrorAmount;
            
            MandatoryDatasetAttributes = Dataflow.Dsd.Attributes
                .Where(a => reportedAttributes.Any(r => r.Code.Equals(a.Code, StringComparison.InvariantCultureIgnoreCase)))
                .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet && a.Base.Mandatory).ToList();

            _errors.Clear();
            _isValid = true;

            if (MandatoryDatasetAttributes.Count == 0)
            {
                return _isValid;
            }

            var allDataSetAttributesProvided = new List<IKeyValue>();

            allDataSetAttributesProvided.AddRange(
                _dataSetAttributeRows
                    .Where(a=> a.Action is StagingRowActionEnum.Merge).SelectMany(a=> a.Attributes));
            
            var attributeList = new List<Attribute>(MandatoryDatasetAttributes);

            // Reduce list of mandatory dataset attributes to the ones not having value provided in the current batch
            attributeList.RemoveAll(m =>
                allDataSetAttributesProvided.Any(a =>
                    a.Concept.Equals(m.Code, StringComparison.CurrentCultureIgnoreCase)));

            if (attributeList.Count == 0)
            {
                return _isValid;
            }

            // Check if there are already database values for the mandatory attributes not loaded in this batch
            await CheckAttributeValuesInDatabase(dotStatDataDb, attributeList, tableVersion, fullValidation, cancellationToken);

            if (attributeList.Count == 0)
            {
                return _isValid;
            }

            // If there are any attributes left in the list, those are the mandatory attributes without value
            foreach (var attribute in attributeList)
            {
                if (_maxErrorCount != 0 && _errors.Count >= _maxErrorCount)
                {
                    break;
                }
                Log.Warn(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MandatoryDatasetAttributeMissing)
                    , null, null, null, null, attribute.Code));
            }

            return _isValid;
        }
        
        protected abstract Task CheckAttributeValuesInDatabase(IDotStatDb dotStatDataDb, List<Attribute> attributeList, DbTableVersion tableVersion, bool fullValidation, CancellationToken cancellationToken);

        private void AddError(ValidationErrorType errorType, IKeyValue keyValue = null, params string[] argument)
        {
            _errors.Add(new KeyValueError()
            {
                Type = errorType,
                Row = 0,
                Coordinate = "",
                Value = keyValue == null ? null : $"{keyValue.Concept}:{keyValue.Code}",
                Argument = argument
            });
        }

        public List<IValidationError> GetErrors()
        {
            return _errors;
        }

        public string GetErrorsMessage()
        {
            var sb = new StringBuilder();

            foreach (var error in _errors)
                sb.AppendLine(error.ToString());

            return sb.ToString();
        }
    }
}