﻿using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class CountValidator : IntegerValidator
    {
        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotCount;

        public CountValidator(decimal? minValue, decimal? maxValue, bool allowNullValue = true) : base(minValue, maxValue, allowNullValue)
        {
        }
    }
}
