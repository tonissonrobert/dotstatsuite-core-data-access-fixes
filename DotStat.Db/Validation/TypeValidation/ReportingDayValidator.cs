﻿using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class ReportingDayValidator : PatternValidator
    {
        private const string ValidationExpression =
            @"^\d{4}\-D(0[0-9][1-9]|[1-2][0-9][0-9]|3[0-5][0-9]|36[0-6])(Z|(\+|\-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?$";

        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotReportingDay;

        public ReportingDayValidator(bool allowNullValue = true) : base (ValidationExpression, allowNullValue)
        {
        }
    }
}
