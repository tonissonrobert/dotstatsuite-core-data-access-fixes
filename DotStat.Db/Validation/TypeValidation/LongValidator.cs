﻿using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class LongValidator : SimpleSdmxMinMaxValueValidator
    {
        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotLong;

        public LongValidator(decimal? minValue, decimal? maxValue, bool allowNullValue = true) : base(minValue, maxValue, allowNullValue)
        {
        }

        protected override bool TryParseDecimalOutFunction(string observationValue, out decimal? decimalForMinMaxCheck, out bool isNegative)
        {
            decimalForMinMaxCheck = null;
            isNegative = false;

            if (!long.TryParse(observationValue, out var longValue))
            {
                return false;
            }

            isNegative = longValue < 0;
            decimalForMinMaxCheck = longValue;

            return true;
        }
    }
}
