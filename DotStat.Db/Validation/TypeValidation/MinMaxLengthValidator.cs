﻿using System;

namespace DotStat.DB.Validation.TypeValidation
{
    public class MinMaxLengthValidator : SimpleSdmxTypeValidator
    {
        private readonly long? _minLength;
        private readonly long? _maxLength;

        public MinMaxLengthValidator(long? minLength, long? maxLength, bool allowNullValue = true) : base(allowNullValue)
        {
            if (_minLength < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(_minLength));
            }

            if (_maxLength < _minLength)
            {
                throw new ArgumentOutOfRangeException(nameof(_maxLength));
            }

            _minLength = minLength;
            _maxLength = maxLength;
        }

        public override bool IsValid(string observationValue)
        {
            ValidationError = Db.Validation.ValidationErrorType.Undefined;

            if ((_maxLength.HasValue && _maxLength.Value == 0) || (AllowNullValue && string.IsNullOrEmpty(observationValue)))
            {
                return true;
            }

            var length = observationValue.Length;

            if (_minLength.HasValue && _minLength.Value > length) 
            {
                ValidationError = Db.Validation.ValidationErrorType.InvalidValueFormatMinLength;

                ExtraErrorMessageParameters = new[] { length.ToString(), _minLength.ToString() };

                return false;
            }

            if (_maxLength.HasValue && _maxLength.Value < length) 
            {
                ValidationError = Db.Validation.ValidationErrorType.InvalidValueFormatMaxLength;

                ExtraErrorMessageParameters = new[] { length.ToString(), _maxLength.ToString() };

                return false;
            }

            return true;
        }
    }
}
