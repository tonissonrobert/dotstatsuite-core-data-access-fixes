﻿using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public abstract class SimpleSdmxTypeValidator : ISdmxTypeValidator
    {
        protected readonly bool AllowNullValue;

        public ValidationErrorType ValidationError { get; protected set; }

        public string[] ExtraErrorMessageParameters { get; protected set; } = null;

        protected SimpleSdmxTypeValidator(bool allowNullValue = true)
        {
            AllowNullValue = allowNullValue;
        }

        public abstract bool IsValid(string observationValue);
    }
}
