# HISTORY

## v22.0.2 (DataDB v8.1.3, CommonDB v3.8)
### Description

### Issues
- [#170](https://gitlab.com/sis-cc/dotstatsuite-documentation/-/issues/170) Updated LICENSE file
- [#403](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/403) Removed concurrency error for requeued requests
- [#528](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/528) Changed transaction and transaction logs datetime fields to store values in UTC
- [#638](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/638) Fixed bug at validation of dataset attributes at Delete action
- [#650](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/650) Fixed check of mandatory attributes at dataflow level
- [#655](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/655) Fixed bug of mandatory OBS_STATUS values disappearing while transferring data from one space to another
- [#658](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/658) Applied bug fix to allow missing mandatory dataset attribute with Replace or Merge action
- [#659](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/659) Fixed bug when only dataflow level attributes are present in a replace action
- [#660](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/660) Fixed missing Measure column issue at Replace action
- [#664](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/664) Fixed 'The given ColumnMapping does not match up with any column in the source or destination' error when uploading data
- [#669](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/669) Fixed duplicated coordinate error at transfer of a dataflow with no dataset attribute value provided 
- [#673](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/673) Fixed wild card deletions


## v22.0.1 (DataDB v8.1.0, CommonDB v3.8)
### Description

### Issues
- [#652](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/652) Fix migration script recreate MSD DF view when DSD does not have time dimension


## v22.0.0 (DataDB v8.1.0, CommonDB v3.8)
### Description

### Issues
- [#53](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/issues/53) Aligned transfer service with dbup in postman tests
- [#126](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/126) Changed recreation of mapping sets and recalculation of actual content constraints to be done always for DSD import/transfer, improved mapping set creating call when dsd has multiple dataflows and  Fixed the bug causing mapping sets not created when import has a DSD reference
- [#402](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/402) Updated nuget references to match with ESTAT v8.18.6
- [#407](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/407) Fixed the bug related to precision of SQL datatype of float and double component types
- [#410](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/410) Updated nuget references to match with ESTAT v8.18.7
- [#596](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/596) Extended transfer service to allow importing and transfering referential metadata referencing a DSD and changed behaviour of transfer/dataflow to only transfer referential metadata attached to dataflow
- [#624](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/624) Fixed the bug causing the second PIT not released
- [#625](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/625) Fixed transfer with non-coded dimension
- [#628](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/628) Added functionality to cancel unfinished jobs when transfer service instance restarts
- [#630](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/630) Fixed bug of dsd's db id not being initialized at calculation of availability 
- [#631](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/631) Fixed init/allMappingsets wrongly failing on external dataflows
- [#637](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/637) Fixed the issue of meta tables not migrated to new structure


## v21.0.0 (DataDB v7.7.0, CommonDB v3.8)
### Description

### Issues
- [#85](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-docker-compose/-/issues/85) Update of docker image description to avoid duplication of configuration documentation
- [#118](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/118) Fixed DbUp issue causing errors logged due to non-existing LAST_STATUS column
- [#338](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/338) Made cleanupDsd function transactional
- [#375](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/375) Advanced data validation should not block data uploads because of invalid DB content
- [#392](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/392) Updated nuget references to match with ESTAT v8.18.4
- [#456](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/456) Changed localization messages for missing dimensions in CSV and non-defined attributes
- [#457](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/457) Improved performance of cleaning mappingsets
- [#576](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/576) Introduced Temporal Tables to support includeHistory and As Of features for Data
- [#598](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/598) Added case sensitive comparison for non-coded component values
- [#601](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/601) Granted CONTROL rights to DotStatWriter role on data schema
- [#603](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/603) Improved performance of init/allmappingsets
- [#615](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/615) Fixed collate error when uploading data with monthly time period date
- Limited no. of parallel execution threads for InitializeAllMappingSets task


## v20.0.1 (DataDB v7.5.0, CommonDB v3.8)
### Description

### Issues
- [#587](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/587) Fixed bug causing actual content constraints being not re-calculated when advanced validation is applied


## v20.0.0 (DataDB v7.5.0, CommonDB v3.8)
### Description

### Issues
- [#46](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/46) Improved texts of AttributeDoesNotBelongToDataset and DimensionDoesNotBelongToDataset localizations
- [#116](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/116) Remediated vulnerabilities of siscc/dotstatsuite-dbup image revealed by docker scout
- [#126](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/126) Implemented non-coded dimension support
- [#158](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/158) Added advanced validations for mandatory attributes at observation level
- [#306](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/306) Implemented support of confidential observations
- [#374](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/374) Updates nuget references to match with ESTAT v8.18.0
- [#377](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/377) Updated nuget references to match with ESTAT v8.18.2
- [#467](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/467) Enhanced the error message for ref. metadata duplicates submissions
- [#534](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/534) Added capability to compress DSD  
- [#545](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/545) Improved management of deleted values
- [#574](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/574) Fix dataflow level attribute import without dimensions
- [#578](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/578) Fixed error at data transfer with advanced validations falsely reporting duplicated observations
- [#579](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/579) Improved execution time of data transfers with basic validations taking much too long
- [#580](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/580) Improved dsd reference error message
- [#583](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/583) Fixed error at transfer of ref. metadata (insert + merge + delete)
- [#584](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/584) Fixed the issue with metadata merges mistakenly doing replaces in certain cases


## v19.0.0 (DataDB v7.4.0, CommonDB v3.8)
### Description

### Issues
- [#30](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/30) Added transfer of deleted data between dataspace
- [#57](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/57) Added updatedAfter feature (timestamp updated/inserted or deleted)
- [#93](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/93) Changed SQL inline string values to sql execution parameters for special values
- [#95](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/95) Fixed treatment of invalid codes of allowed CC at actual CC creation
- [#99](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/99) Added last_updated info to all storage levels of components for updatedAfter feature
- [#100](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/100) Fixed the View typo that prevented migration when there was Dataset lvl attribute with codelist
- [#105](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/105) Improved performance of observation validation function
- [#106](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/106) Added memory cache for validated time values
- [#109](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/109) Fixed migration SQL script of metadata dataflow views
- [#112](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/112) DbUp base docker image changed from debian to alpine
- [#124](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/124) Removed un-used dependency which contains CRITICAL security issues
- [#134](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/134) Fixed issue of attribute deletion not propagated at transfers for dimension-level attributes
- [#144](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/144) Enabled any length of non-coded attributes when limit is over 4000; Correction of link URL in maximum attribute length related messages
- [#258](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/258) Added updatedAfter parameter in transfer service for dataspace transfers
- [#277](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/277) SDMX-XML v2.0 readers support deletion action features
- [#300](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/300) Implemented storage of updatedAfter information for referential metadata
- [#313](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/313) Updated nuget references to match with ESTAT v8.13.0
- [#314](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/314) Fix null reference bug in case of dataflow (related to a DSD) without attributes
- [#325](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/325) Updated nuget references to match with ESTAT v8.15.0
- [#331](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/331) Updated nuget references to match with ESTAT v8.15.1
- [#335](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/335) Added new implementation of 'getdataflow' based on base structures
- [#352](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/352) Updated metadata delete and merge actions
- [#355](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/355) Updated nuget references to match with ESTAT v8.16.0
- [#361](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/361) Updated nuget references to match with ESTAT v8.17.0
- [#363](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/363) Upgraded migration criteria to update datasets and mappingsets; Fixed mappingset time_pre_formated bug for the very first time a dataflow is initialized
- [#365](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/365) Changed validators to scoped
- [#427](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/427) Culture specific csv import
- [#434](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/434) Coded ref. metadata treated as non-coded
- [#446](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/446) Enhanced intentionally missing feature
- [#451](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/451) Added deleted and replaced values for transfer of data across data spaces
- [#453](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/453) Fixed initialisation of dataflow
- [#460](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/460) Improved actual content constraints and mappingsets management
- [#464](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/464) Added new localized text for failed init/dataflow 
- [#465](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/465) Fixed FATAL error in data transfer when there are no dataset attributes
- [#466](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/466) Introduced Replace action
- [#469](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/469) Iterative merge per 'Delete' instructions 
- [#470](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/470) Store and retrieve (through updatedAfter) original 'Delete' instructions
- [#471](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/471) Auto-delete fully empty observations, attributes at partial keys and ref. metadata
- [#484](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/484) Added missing time dimension check for metadata import
- [#490](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/490) Aligned Replace with Merge for higher-level attributes and refactored SQL merge logic
- [#492](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/492) Fixed the issue of transfers failing when related DSD has no dimension/group attributes
- [#497](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/497) Fixed the issue of deleting time-series attr. also deletes group attributes
- [#501](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/501) Fixed the issue of replacing the value of a Dataflow level attribute deletes all content
- [#502](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/502) Metadata time wildcard fix
- [#504](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/504) Timestamp is not updated and ACC is not recalculated when there is no impact on the stored values
- [#505](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/505) Fixed the bug of deleting higher level attributes did not delete them but skip them
- [#506](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/506) Wildcard deletions also respect the allowed content constraint
- [#508](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/508) Metadata validation updates
- [#510](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/510) Harmonized datetime treatment by using only .net system time in all code parts (and not SQL server time)
- [#511](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/511) Fix dataflow attribute delete
- [#514](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/514) Fixes applied on transferring metadata
- [#517](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/517) Transfer of filtered obs. values must include attributes attached at all higher levels
- [#518](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/518) The LAST_UPDATED column of updatedAfter feature is stored as UTC time
- [#536](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/536) Switched-off metadata dim values returned as a special character instead of null (empty string) 
- [#537](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/537) Improved replace part of transfer data/metadata accross spaces
- [#541](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/541) Updated metadata dim validation
- [#542](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/542) MSD info is populated from structure db when not returned from the file readers
- [#543](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/543) Fixed transfer of group/dimension attributes in case there are no observations in source data space
- [#546](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/546) Change default ref. metadata length from 4000 to MAX in the storage type
- [#548](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/548) Enhanced error message for duplicated observations
- [#550](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/550) Upgrade of google log4net lib as the old one was failing in alpine linux 
- [#552](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/552) Changed the special value to indicate switched-off dimension from "-" to "~"
- [#553](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/553) Changed the values to represent intentionally missing 'NaN' for Float and double and "#N/A" for textual representation.
- [#554](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/554) Fixed bug of mapping set being not created after unsuccessful import
- [#556](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/556) Fixed null-reference error when trying to delete data with basic validation
- Updated license information


## v18.0.7 (DataDB v7.0.1, CommonDB v3.8)
### Description

### Issues
- [#126](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/126) Fix for log records of transactions appearing in two different dataspaces
- [#563](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/563) Private repository replaced to Gitlab


## v18.0.6 (DataDB v7.0.1, CommonDB v3.8)
### Description

### Issues
- [#450](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/450) Fix for non-unique component ID accross DSD & referenced MSD
- [#459](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/459) Applied DatabaseCommandTimeoutInSec (max if multiple dataspaces) to SQL commands run by Eurostat mappingstore libs run by dataaccess/transfer service


## v18.0.5 (DataDB v7.0.1, CommonDB v3.8)
### Description

### Issues
- [#453](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/453) Fix initialising dataflow
- [#444](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/444) Transaction timeout
- [#448](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/448) Primary measure as codelist, bulkinsert convert bug


## v18.0.4 (DataDB v7.0.1, CommonDB v3.8)
### Description

### Issues
- [#94](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/94) Script added to DbUp tool to regenerate all DSD views
- [#443](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/443) Fixed issue of files with duplicates being saved skipping duplicated rows with no error message
- [#444](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/444) Fixed time-out issue at data upload of 70+ million observations


## v18.0.3 (DataDB v7.0.0, CommonDB v3.8)
### Description

### Issues
- [#440](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/440) Fixed error of false duplicates reported in advance validation


## v18.0.2 (DataDB v7.0.0, CommonDB v3.8)
### Description

### Issues
- [#286](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/286) TIME_PRE_FORMATED table cleanup fix
- [#289](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/289) References updated to match with NSI WS v8.12.2
- [#435](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/435) Fixed errors with missing referenced dimensions and dataflow attribute management
- [#437](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/437) Fix of index creation applied on staging table in case of large file imports


## v18.0.1 (DataDB v7.0.0, CommonDB v3.8)
### Description

### Issues
- [#430](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/430) Revert the isolation level to READ COMMITTED in connections with no transaction
- [#431](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/431) Fix performance issue with the merge statement from staging to fact table


## v18.0.0 (DataDB v7.0.0, CommonDB v3.8)
### Description

### Issues
- [#91](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/91) Fix multiple contradictory values error at dataset attributes
- [#127](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/127) Introduced data DELETE operation
- [#251](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/251) References updated to match with NSI WS v8.12.0
- [#281](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/281) References updated to match with NSI WS v8.12.1
- [#285](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/285) Allow views to return higher level attribute values when there are no observation level component rows
- [#397](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/397) Implemented MERGE action for CSV v2 data imports
- [#402](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/402) Update test file for the improved detection of data or metadata content of a CSV v2 file
- [#409](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/409) Fixed transaction id in time-out log message
- [#410](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/410) No exception is thrown at init/allMappingsets method in case of no DSDs or no dataflows in data space
- [#416](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/416) Fix bug in case there are no dataset attribute values to modify
- [#424](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/424) Fix issue when deleting specific years
- [#425](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/425) Fixed import of first dataset level metadata attribute
- [#426](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/426) Updated authentication to support ADFS


## v17.0.4 (DataDB v6.2.1, CommonDB v3.8)
### Description

### Issues
- [#387](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/387) Add optional SMTP HFrom header configuration
- [#90](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/90) Grant view server state command is executed with sysadmin role only, dbup tool version printed to console


## v17.0.3 (DataDB v6.2.1, CommonDB v3.8)
### Description

### Issues
- [#403](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/403) Fix creation of actual-CC


## v17.0.2 (DataDB v6.2.1, CommonDB v3.8)
### Description

### Issues
- [#379](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/379) Add MailSent
- [#382](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/382) Cleanup of orphaned codelists
- [#383](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/383) Fix "already added" in MappingStoreDataAccess, Add missing application name in structdbconnetionString
- [#386](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/386) Avoid re-accessing the same column


## v17.0.1 (DataDB v6.2.1, CommonDB v3.8)
### Description

### Issues
- Disabled resource auto detection of Google logger in DbUp


## v17.0.0 (DataDB v6.2.1, CommonDB v3.8)
### Description

### Issues
- [#83](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/83) Upgraded from .NET Core 3.1 to .NET 6
- [#86](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/86) The method cleanup/dsd deletes the mapping sets and actual content constraint of the dataflows
- [#87](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/87) Added connection close fix and isolation level fix
- [#245](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/245) Mappingset cleanup transaction is executed in dataflow scope
- [#251](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/251) ESTAT references updated to v8.11.0
- [#267](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/267) Added log4net google appender
- [#277](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/277) Added batchNumber to logs for staging data and metadata
- [#285](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/285) Always set the Actual CC validFrom date
- [#291](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/291) Introduced cancellation tokens and readonly db connections
- [#317](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/317) Fix for performance degradation of dataflow view when adding a value for a dataset-level attribute
- [#353](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/353) Modify TryNewTransaction to allow requeueing requests
- [#361](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/361) Initialize dsd object of dataflows with data management properties
- [#362](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/362) Updated msd cleanup
- [#363](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/363) Fix tryNewTransaction when first request for a dsd is initdataflow
- [#369](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/369) Fix of dataset attribute import
- [#373](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/373) Set QUEUED_DATETIME value to EXECUTION_START for transaction rows existed in db before the addition of the field QUEUED_DATETIME


## v16.1.1 (DataDB v6.1.1, CommonDB v3.8)
### Description

### Issues
- [#269](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/269) Correction of some transfer messages
- [#350](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/350) Introduce ICache param to mappingstoredataaccess


## v16.1.0 (DataDB v6.1.1, CommonDB v3.8)
### Description

### Issues
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/278) Mark transactions as aborted or closed
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/319) Import from URL made async 
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/320) Bug fix formatting of the email summary 
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/83) Upgrade from netstandard2.1/netcoreapp3.1 to net6.0 
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/114) Support for NaN as observation 
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/304) Manage unhandled exceptions
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/147) add servicebus, added classes for messaging service (part 1)
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/182) Fix of dataset column mapping issue 


## v16.0.1 (DataDB v6.1.0, CommonDB v3.8)
### Description

### Issues
- [#321](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/321) Fix for issue of data transactions failing with dataflows supporting ref.metadata


## v16.0.0 (DataDB v6.1.0, CommonDB v3.8)
### Description
- [#310](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/310) block all transactions when cleanup process
- [#316](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/316) Add HttpClientTimeOut 
- [#82](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/82) Add validation of time dimension for referential metadata, fix identification Dsd level metadata rows 
- [#306](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/306) Fix ACC validity update at metadata imports 
- [#234](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/234) References updated to NSI v8.9.2 
- [#130](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/130) Improvements to firstNObservations and lastNObservations queries 
- [#302](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/302) Fix of object ref not set error at simple transaction creation 
- [#305](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/305) fix Hierarchical Referential Metadata Attributes are not Imported 
- [#603](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-explorer/-/issues/603) Dataset Attribute parameter passed as NVarchar 
- [#224](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/224) References updated to NSI v8.9.1 h
- [#296](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/296) Correct the transfer log messages for Referential Metadata transactions
- [#293](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/293) Add a proper error description when uploading metadata file to a structure without annotation link to MSD
- [#82](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/82) Fix MetadataDataFlow view period_start and period_end add datetime support 
- [#176](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/176) Add source dataspace and data source fields to transaction logs 
- [#295](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/295) Remove ORDER BY for metadata Mappingsets 
- [#81](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/81) added mappingsets for metadata queries when DSD has MSD reference
- [#80](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/80) Metadata storage implementation 


## v15.0.0 (DataDB v5.6.3, CommonDB v3.8)
### Description

### Issues
- [#184](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/184) Improved management of time preformatted entries and deletion of mapping set relates objects from database 
- [#230](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/230) SDMX-CSV 2.0.0 (meta)data upload 
- [#228](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/228) One of the multiple attributes values at same series key is taken at basic validation  

## v14.0.0 (DataDB v5.6.3, CommonDB v3.8)
### Description

### Issues
- [#174](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/174) References updated to match Eurostat NSI v8.7.1
- [#189](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/189) Manage Time in group attributes - part 2


## v13.0.1 (DataDB v5.6.3, CommonDB v3.8)
### Description

### Issues
- [#76](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/76) Errors found in the AllowMissingComponents SQL migration script for the recreation of the DSD/DF views
- [#77](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/77) Errors found in the ChangeFactTableIndexes SQL migration script for the recreation of the Fact table indexes


## v13.0.0 (DataDB v5.6, CommonDB v3.8)
### Description

### Issues
- [#33](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/33) Added TokenUrl param to auth config
- [#75](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/75) Change primary key from PERIOD_SDMX to PERIOD_START and PERIOD_END, fix of semantic error displayed when time period is invalid
- [#113](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/113) Support REPORTING_YEAR_START_DAY attribute, Time range format, date + time format, fixed NumberStyles used at float and double, usage of SUPPORT_DATETIME annotation added
- [#161](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/161) References updated to match Eurostat NSI v8.5.0
- [#178](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/178) Added new localisation keys for file management
- [#210](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/210) New date format applied on PIT release and restoration dates
- [#212](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/212) Added missing Primary measure representation warning
- [#214](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/214) Fix of codelist item ids not in consecutive order issue
- [#215](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/215) Allow non reported components
- [#223](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/223) Changed localization text for bulk copy notifications and Improve merge performance with full details
- [#226](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/226) Added number of obs to pre-generated actual content constraints, fix of NumberStyle for float and double during observation validation
- [#231](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/231) Fill order_by information for mappingsets, recreate DSD and DataFlow views include SID column
- [#237](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/237) Updated mapping set creation function to use new TIME_PRE_FORMATED mapping
- [#242](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/242) Data db connection info not saved to MSDB depending on ApplyEmptyDataDbConnectionStringInMSDB configuration parameter
- [#245](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/245) Fix of finding reported attributes in DataflowMetadataBuilder
- [#251](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/251) Corrected the creation of staging table, use fact table info to create time columns, when the fact table exists; fix missing attributes in destination during transfer


## v12.4.0 (DataDB v5.3, CommonDB v3.8)
### Description

### Issues
[#72](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/72) Creation of read-only user added to DbUp
[#116](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/116) Added support for Azure SQL Database and Azure SQL Managed Instance to DbUp


## v12.3.0
### Description

### Issues
[#70](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/70) Password change added to DbUp 
[#71](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/71) Replaced sql CONCAT_WS function with CONCAT function
[#20](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-kube-core-rp/-/issues/20) Added Devops DB config tool 
[#123](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/123) Add basic and full validations 
[#174](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/174) Create transaction item for init/allMappingsets method  
[#123](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/123) Remove primary key in staging table  
[#4](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/4) Update Transfer service error messages 
[#198](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/198) Initialize empty mappingsets and actual content constraints for the first data upload, even if the transaction fails 
[#205](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/205) Fix bug, actual content constraint not updated after data transfer 
[#202](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/202) Fix of wrong content constraint start date update issue


## v12.2.0
### Description

### Issues
[#16](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/16) Bugfix, 4.0 Time dimension without codelist. 


## v12.1.0
### Description

### Issues
[#69](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/69) Bugfix for issue, dbup changes target db to single user mode at start and sets back to multiuser on finishs. 


## v12.0.0
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

This version is a major update with breaking changes to the both code and DataDB. 

### DB changes: 

DataDB v5.0
 
- SQL data type of ENUM_ID column in management.ENUMERATIONS table changed to bigint
- SQL data type of ENUM_ID column in management.COMPONENT table changed to bigint
- MIN_LENGTH column added to management.COMPONENT table
- MAX_LENGTH column added to management.COMPONENT table
- PATTERN column added to management.COMPONENT table
- MIN_VALUE column added to management.COMPONENT table
- MAX_VALUE column added to management.COMPONENT table
- Primary measure component inserted into management.COMPONENT table for all existing DSDs with ENUM_ID = 7 (float)
- DSD_OBS_VALUE_DATA_TYPE column deleted from management.ARTEFACT table
- SQL data type of data.FACT_{dsd ID}_{table version}.VALUE column in FACT table depends on primary measure's SDMX representation in the related DSD (see documentation for further info)
- data.ATTR_{dsd Id}_{table version}_DIMGROUP table deleted
- data.ATTR_{dsd Id}_{table version} table added
- data.FILT_{dsd ID}.ROW_ID column deleted if DSD contains less than 33 non-time dimensions
- data.FACT_{dsd ID}_{table version}.DIM_TIME column deleted from FACT table
- data.FACT_{dsd ID}_{table version}.PERIOD_SDMX column added to FACT table
- data.FACT_{dsd ID}_{table version}.PERIOD_START column added to FACT table
- data.FACT_{dsd ID}_{table version}.PERIOD_END column added to FACT table

CommonDB v3.7
- Grant SELECT permission on table dbo.DB_VERSION

### Issues
[#96](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/96) Cleanup mappingsets updated. 
<BR>[#168](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/168) Fix codelist mapping identification of DSD components. 
<BR>[#25](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/25) Bugfix, add script for granting select permission.
<BR>[#60](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/60) Improve error messages for mistakes in the csv layout. 
<BR>[#68](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/68) Bugfix time format. 
<BR>[#124](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/124) Non-numeric measure type implementation. 
<BR>[#69](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/69) dbup changes target db to single user mode at start and sets back to multiuser on finish. 
<BR>[#159](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/159) Fix string.Format exceptions and time dim check. 
<BR>[#67](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/67) Add ExecutionTimeout in DbUp. 
<BR>[#]() DB documention update
<BR>[#159](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/159) Manage Time in group attribute. 
<BR>[#84](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/84) Support for DSD without Time dimension. 
<BR>[#57](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/57) Add localization key sdmx-ml groups before series. 
<BR>[#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/167) Validation of data database version. 

## v11.0.7 (MSDB v6.12, DataDB v3.5) 
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

This version is a major update with breaking changes to the both code and DataDB. 

### Issues
[#66](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/66) Fix of bug in SqlManagementRepository.CleanUpDsd. 
<BR>[#65](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/65) Add dbup functionality to clear orphan DSD/DF records in Artefact table, for which there is not FILTER/FACT tables

## v11.0.0 (MSDB v6.12, DataDB v3.5) 
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

This version is a major update with breaking changes to the both code and DataDB. 

### Issues
- [#63](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/63) Fix of version issue at querying mapping sets and log feature fixes.
- [#103](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/103) References updated to match Eurostat NSI v8.1.2
- [#110](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/110) Add allowed content constraint check for attributes.
- [#160](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/160) Refactor validation and exception handling.
- [#165](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/165) Bug fix for fix allDataflows fails with a Timed Out Error.
- [#163](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/163) Fix of attribute related issues in DbUp script updating dataflow views.
- [#63](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/63) Fix Error executing generated SQL and populating SDMX model.
- [#161](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/161) Fix dataflow views with non mandatory dsd level attributes.
- [#120](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/120) Feature to consult the status of the data imports/transactions.
- [#49](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/49) Automatically create mapping sets.
- [#157](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/157) Validate when mappingset valid to date is datemax, set it to datemax - 1 second.
- [#20](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/20) Update of AdminRole permissions added.
- [#108](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/108) Use Estat PermissionType.
- [#143](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/143) Detailed import summary added.
- [#48](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/48) Automatically create data database sql views for dsd and dataflows.
- [#53](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/53) Grant permision to create view to dotstatwriter role.
 


## v10.2.0 (MSDB v6.10, DataDB v2.1) 
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/53

## v10.1.0 (MSDB v6.9, DataDB v2.1) 
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/102
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/101
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/107
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/100
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/47

## v10.0.5 202006-08 (MSDB v6.9,DataDB v2.1)
- Bugfix related to https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/79

## v10.0.0 202006-08 (MSDB v6.9,DataDB v2.1)
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/111
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/46
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/101
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/1
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/96
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/94
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/78
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/29
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/95
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/79
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/83
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/93

## v8.0.3 2020-04-18  (MSDB v6.8, DataDB v2.1)

- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/47
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/84
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/62
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/103

##  v7.3.1 2020-03-30 (MSDB v6.8, DataDB v2.1)
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/88 + bugfix

##  v7.2.1 2020-03-30 (MSDB v6.8, DataDB v2.1)
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/88

 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/74
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/76
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/86
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/issues/104
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/85
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/42

##  v7.1.1 2020-03-20 (MSDB v6.8, DataDB v2.1)
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/issues/43
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/74
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/76
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/86
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/issues/104
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/85
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/42

##  v7.0.1 2020-03-20 (MSDB v6.8, DataDB v2.1)
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/issues/37
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/39
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/issues/34

##  v6.2.1 2020-02-13 (MSDB v6.8, DataDB v2.1)
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/36

##  v6.1.1 2020-01-31 (MSDB v6.7, DataDB v2.1)

- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/35

##  v6.0.1 2020-01-22 (MSDB v6.7, DataDB v2.1)

- sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/66

##  v5.2.6 2020-01-22 (MSDB v6.7, DataDB v2.1)

- sis-cc/.stat-suite/dotstatsuite-core-common/issues/102