﻿using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
using System;
using System.Collections.Generic;

namespace DotStat.MappingStore
{
    /// <summary>
    /// This interface is not thread safe and should be used in a Scoped|Transient context
    /// </summary>
    public interface IMappingStoreDataAccess
    {
        Dataflow GetDataflow(
            string dataSpaceId, 
            string agencyId, 
            string dataflowId, 
            string version, 
            bool throwErrorIfNotFound = true, 
            ResolveCrossReferences resolveCrossReferences = ResolveCrossReferences.ResolveExcludeAgencies, 
            bool useCache = true
        );
        
        Dataflow GetDataflow(
            string dataSpaceId, 
            IDataflowObject dataflowObject, 
            IDataStructureObject dataStructure, 
            IMetadataStructureDefinitionObject metadataStructureObject, 
            ResolveCrossReferences resolveCrossReferences = ResolveCrossReferences.ResolveExcludeAgencies, 
            bool useDataflowCache = true,
            bool useMsdCache = false
        );

        Dsd GetDsd(string dataSpaceId, string agencyId, string dsdId, string version, bool throwErrorIfNotFound = true, ResolveCrossReferences resolveCrossReferences = ResolveCrossReferences.ResolveExcludeAgencies, bool useCache = true);
        Dsd GetDsd(string dataSpaceId, IDataStructureObject dataStructure, IMetadataStructureDefinitionObject metadataStructureObject, bool useDsdCache = true, bool useMsdCache = false);

        ISdmxObjects GetDsd(string dataSpaceId, string agencyId, string dsdId, string version);
        ISet<IDataStructureObject> GetDataStructures(string dataSpaceId);
        ISet<IDataflowObject> GetDataflows(string dataSpaceId);
        ISdmxObjectRetrievalManager GetRetrievalManager(string dataSpaceId);
        void DeleteActualContentConstraint(string dataSpaceId, string agencyId, string id, string version, char targetTable, TargetVersion targetVersion);
        public void DeleteAllActualContentConstraints(string dataSpaceId, Dataflow dataflow);
        void SaveContentConstraint(string dataSpaceId, Dataflow dataflow, IList<IKeyValuesMutable> cubeRegion, bool? includedCubeRegion, char targetTable, TargetVersion targetVersion, DateTime liveAccStartDate, long? obsCount = null);
        public void UpdateValidityOfContentConstraints(string dataSpaceId, Dataflow dataflow, char targetTable, TargetVersion targetVersion);
        (bool isAnyMappingSet, bool isAnyUserCreatedMappingSet) HasMappingSet(string dataSpaceId, Dataflow dataFlow);
        bool DeleteDataSetsAndMappingSets(string dataSpaceId, Dataflow dataFlow, bool deleteMsdObjectsOnly); 
        bool CreateOrUpdateMappingSet(string dataSpaceId, Dataflow dataFlow, char targetVersion, DateTime? validFrom, DateTime? validTo, MappingSetParams @params);
        HashSet<string> GetMigratedMappingSets(string dataSpaceId);
        bool IsMigrated(Dataflow dataflow, HashSet<string> migratedMappingSets);
    }
}