﻿using CommandLine;

namespace DotStat.Devops.Db.Config.Options
{
    [Verb("upgrade", HelpText = "Upgrade the specified database, e.g.: DotStat.Devops.Db.Config.exe upgrade --connectionString \"Server=localhost;Database=master;Trusted_connection=true\"")]
    public class UpgradeOption
    {
        [Option("connectionString", Required = true, HelpText = "The connection string for the database")]
        public string ConnectionString { get; set; }
    }
}
