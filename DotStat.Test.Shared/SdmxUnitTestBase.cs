﻿using System;
using System.Linq;
using System.Threading;
using DotStat.Common.Configuration;
using DotStat.Common.Localization;
using DotStat.Db;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Test.Moq;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;

namespace DotStat.Test
{
    public abstract class SdmxUnitTestBase : UnitTestBase
    {
        /// <summary>
        /// Mapping Store Data access
        /// </summary>
        protected readonly TestMappingStoreDataAccess MsAccess;

        /// <param name="dfPath">Filepath to Dataflow structure definition</param>
        protected SdmxUnitTestBase(string dfPath = null)
        {
            MsAccess = new TestMappingStoreDataAccess(dfPath ?? "sdmx/264D_264_SALDI+2.1.xml");
        }

        /// <summary>
        /// Returns dataflow defined in the constructor
        /// </summary>
        /// <returns></returns>
        protected Dataflow GetDataflow() => MsAccess.GetDataflow();

        /// <summary>
        /// Returns dataflow from the provided path
        /// </summary>
        /// <param name="dfPath">Filepath to Dataflow structure definition</param>
        /// <returns></returns>
        protected Dataflow GetDataflow(string dfPath) => SdmxParser.ParseDataFlowStructure(MsAccess.GetSdmxObjects(dfPath));

        /// <summary>
        /// Returns dsd defined in the constructor
        /// </summary>
        /// <returns></returns>
        protected Dsd GetDsd() => MsAccess.GetDsd();

        /// <summary>
        /// Returns dsd from the provided path
        /// </summary>
        /// <param name="dsdPath">Filepath to Dsd structure definition</param>
        /// <returns></returns>
        protected Dsd GetDsd(string dsdPath) => SdmxParser.ParseDsdStructure(MsAccess.GetSdmxObjects(dsdPath));

        /// <summary>
        /// Returns IDataQuery object, constructed from the rest query. Rest query should use the same dataflow as defined in constructor.
        /// </summary>
        /// <param name="restQuery"></param>
        /// <returns>IDataQuery</returns>
        protected IDataQuery GetQuery(string restQuery = "data/264D_264_SALDI/.IT....Y0-14.../?startPeriod=2011&endPeriod=2014")
        {
            var queryParseManager = new DataQueryParseManager(SdmxSchemaEnumType.VersionTwoPointOne);
            return queryParseManager.ParseRestQuery(restQuery, MsAccess.GetRetrievalManager("test"));
        }

    }

    public abstract class UnitTestBase
    {
        protected BaseConfiguration Configuration;
        protected CancellationToken CancellationToken = CancellationToken.None;

        protected UnitTestBase()
        {
            Configuration = GetConfiguration().Get<BaseConfiguration>();
            // Dependencies configuration ------------------
            LocalizationRepository.Configure(Configuration);
            //----------------------------------------------
        }

        protected IConfigurationRoot GetConfiguration(string path = "config")
        {
            var builder = new ConfigurationBuilder();

            foreach (var json in System.IO.Directory.GetFiles(path, "*.json"))
                builder.AddJsonFile(json);

            return builder.Build();
        }

        protected ReportedComponents GetReportedComponents(Dataflow dataflow)
        {
            return GetReportedComponents(dataflow.Dsd);
        }

        protected ReportedComponents GetReportedComponents(Dsd dsd)
        {
            return new ReportedComponents
            {
                DatasetAttributes = dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet || a.Base.AttachmentLevel == AttributeAttachmentLevel.Null).ToList(),
                SeriesAttributesWithNoTimeDim = dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group || a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                    .Where(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                    .ToList(),
                ObservationAttributes = dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation 
                    || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList(),
                Dimensions = dsd.Dimensions.ToList(),
                TimeDimension = dsd.TimeDimension,
                IsPrimaryMeasureReported = true
            };
        }

    }
}
