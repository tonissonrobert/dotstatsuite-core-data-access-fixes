﻿using DotStat.DbUp;
using DotStat.DbUp.Options;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public sealed class DbUpTests : SdmxUnitTestBase
    {
        private readonly UpgradeOption _upgradeOptionWithROloginNameDataDb;
        private readonly UpgradeOption _upgradeOptionWithoutROloginNameDataDb;

        private readonly UpgradeOption _upgradeOptionWithROloginNameMappingStoreDb;
        private readonly UpgradeOption _upgradeOptionWithoutROloginNameMappingStoreDb;

        private readonly UpgradeOption _upgradeOptionWithROloginNameCommonDb;
        private readonly UpgradeOption _upgradeOptionWithoutROloginNameCommonDb;

        private readonly UpgradeOption _upgradeOptionWithCommonDbWithoutDbaScripts;

        /// <summary>
        /// Tests Predicate construction for Dataflow with Constraints
        /// </summary>
        public DbUpTests()
        {
            _upgradeOptionWithROloginNameDataDb = new UpgradeOption()
            {
                AlterPassword = true,
                DataDb = true,
                LoginName = "TestUser",
                LoginPwd = "TestPwd",
                ROloginName = "TestROUser",
                ROloginPwd = "TestROPwd",
                ConnectionString = "Server=db;Database=DataDb;User=TestUser;Password=TestPwd"
            };

            _upgradeOptionWithoutROloginNameDataDb = new UpgradeOption()
            {
                AlterPassword = true,
                DataDb = true,
                LoginName = "TestUser",
                LoginPwd = "TestPwd",
                ConnectionString = "Server=db;Database=DataDb;User=TestUser;Password=TestPwd"
            };

            _upgradeOptionWithROloginNameMappingStoreDb = new UpgradeOption()
            {
                AlterPassword = true,
                MappingStoreDb = true,
                LoginName = "TestUser",
                LoginPwd = "TestPwd",
                ROloginName = "TestROUser",
                ROloginPwd = "TestROPwd",
                ConnectionString = "Server=db;Database=MappingStoreDb;User=TestUser;Password=TestPwd"
            };

            _upgradeOptionWithoutROloginNameMappingStoreDb = new UpgradeOption()
            {
                AlterPassword = true,
                MappingStoreDb = true,
                LoginName = "TestUser",
                LoginPwd = "TestPwd",
                ConnectionString = "Server=db;Database=MappingStoreDb;User=TestUser;Password=TestPwd"
            };

            _upgradeOptionWithROloginNameCommonDb = new UpgradeOption()
            {
                AlterPassword = true,
                CommonDb = true,
                LoginName = "TestUser",
                LoginPwd = "TestPwd",
                ROloginName = "TestROUser",
                ROloginPwd = "TestROPwd",
                ConnectionString = "Server=db;Database=CommonDb;User=TestUser;Password=TestPwd"
            };

            _upgradeOptionWithoutROloginNameCommonDb = new UpgradeOption()
            {
                AlterPassword = true,
                CommonDb = true,
                LoginName = "TestUser",
                LoginPwd = "TestPwd",
                ConnectionString = "Server=db;Database=CommonDb;User=TestUser;Password=TestPwd"
            };

            _upgradeOptionWithCommonDbWithoutDbaScripts = new UpgradeOption()
            {
                CommonDb = true,
                LoginName = "TestUser",
                LoginPwd = "TestPwd",
                ConnectionString = "Server=db;Database=CommonDb;User=TestUser;Password=TestPwd",
                WithoutDbaScripts = true
            };
        }

        [TestCase("", false, false, false, false)]
        [TestCase("    ", false, false, false, false)]
        [TestCase(null, false, false, false, false)]
        [TestCase("DotStat.DbUp.MsSql.DataDb.2021-01-01-01.SqlScrip1.sql", false, false, false, false)]
        [TestCase("DotStat.DbUp.MsSql.CommonDb.2021-01-01-01.SqlScrip1.sql", false, false, false, false)]
        [TestCase("DotStat.DbUp.MsSql.MappingStoreDb.2021-01-01-01.SqlScrip1.sql", false, false, false, false)]
        [TestCase("DotStat.DbUp.MsSql.Master.0100.CreateOrAlterLogin.sql", false, false, false, false)]
        [TestCase("DotStat.DbUp.MsSql.AlwaysRunLast.0100.CreateOrAlterDbUser.sql", true, true, true, false)]
        [TestCase("DotStat.DbUp.MsSql.AlwaysRunLast.0500.SetVersion.sql", true, false, true, true)]
        [TestCase("DotStat.DbUp.MsSql.AlwaysRunLast.9900.SetMultiUserMode.sql", true, false, true, false)]
        [TestCase("DotStat.DbUp.MsSql.AlwaysRunLast.1234.TestScript.sql", true, true, true, false)]
        [TestCase("DotStat.DbUp.MsSql.AlwaysRunFirst.1234.TestScript.sql", false, false, false, false)]
        public void TestAlwaysRunLastScriptWithROloginName(string fileName, bool expectedResultDataDb, bool expectedResultMappingStoreDb, bool expectedResultCommonDb, bool expectedResultWithoutDba)
        {
            Assert.AreEqual(expectedResultDataDb,
                DbUp.Program.FilterAlwaysRunLastScript(fileName, _upgradeOptionWithROloginNameDataDb), $"DataDb -> {fileName}");

            Assert.AreEqual(expectedResultMappingStoreDb,
                DbUp.Program.FilterAlwaysRunLastScript(fileName, _upgradeOptionWithROloginNameMappingStoreDb), $"MappingStoreDb -> {fileName}");

            Assert.AreEqual(expectedResultCommonDb,
                DbUp.Program.FilterAlwaysRunLastScript(fileName, _upgradeOptionWithROloginNameCommonDb), $"CommonDb -> {fileName}");

            Assert.AreEqual(expectedResultWithoutDba,
                DbUp.Program.FilterAlwaysRunLastScript(fileName, _upgradeOptionWithCommonDbWithoutDbaScripts), $"CommonDb -> {fileName}");
        }

        [TestCase("", false, false, false, false)]
        [TestCase("    ", false, false, false, false)]
        [TestCase(null, false, false, false, false)]
        [TestCase("DotStat.DbUp.MsSql.DataDb.2021-01-01-01.SqlScrip1.sql", false, false, false, false)]
        [TestCase("DotStat.DbUp.MsSql.CommonDb.2021-01-01-01.SqlScrip1.sql", false, false, false, false)]
        [TestCase("DotStat.DbUp.MsSql.MappingStoreDb.2021-01-01-01.SqlScrip1.sql", false, false, false, false)]
        [TestCase("DotStat.DbUp.MsSql.AlwaysRunLast.0100.CreateOrAlterDbUser.sql", true, true, true, false)]
        [TestCase("DotStat.DbUp.MsSql.AlwaysRunLast.0150.CreateOrAlterReadOnlyDbUser.sql", false, false, false, false)]
        [TestCase("DotStat.DbUp.MsSql.AlwaysRunLast.0500.SetVersion.sql", true, false, true, true)]
        [TestCase("DotStat.DbUp.MsSql.AlwaysRunLast.9900.SetMultiUserMode.sql", true, false, true, false)]
        [TestCase("DotStat.DbUp.MsSql.AlwaysRunLast.1234.TestScript.sql", true, true, true, false)]
        [TestCase("DotStat.DbUp.MsSql.AlwaysRunFirst.1234.TestScript.sql", false, false, false, false)]
        public void TestAlwaysRunLastScriptWithoutROloginName(string fileName, bool expectedResultDataDb, bool expectedResultMappingStoreDb, bool expectedResultCommonDb, bool expectedResultWithoutDba)
        {
            Assert.AreEqual(expectedResultDataDb,
                DbUp.Program.FilterAlwaysRunLastScript(fileName, _upgradeOptionWithoutROloginNameDataDb), $"DataDb -> {fileName}");

            Assert.AreEqual(expectedResultMappingStoreDb,
                DbUp.Program.FilterAlwaysRunLastScript(fileName, _upgradeOptionWithoutROloginNameMappingStoreDb), $"MappingStoreDb -> {fileName}");

            Assert.AreEqual(expectedResultCommonDb,
                DbUp.Program.FilterAlwaysRunLastScript(fileName, _upgradeOptionWithoutROloginNameCommonDb), $"CommonDb -> {fileName}");

            Assert.AreEqual(expectedResultWithoutDba,
                DbUp.Program.FilterAlwaysRunLastScript(fileName, _upgradeOptionWithCommonDbWithoutDbaScripts), $"CommonDb -> {fileName}");
        }

        [TestCase("", false, false, false, false)]
        [TestCase("    ", false, false, false, false)]
        [TestCase(null, false, false, false, false)]
        [TestCase("DotStat.DbUp.MsSql.DataDb.2021-01-01-01.SqlScrip1.sql", false, false, false, false)]
        [TestCase("DotStat.DbUp.MsSql.CommonDb.2021-01-01-01.SqlScrip1.sql", false, false, false, false)]
        [TestCase("DotStat.DbUp.MsSql.MappingStoreDb.2021-01-01-01.SqlScrip1.sql", false, false, false, false)]
        [TestCase("DotStat.DbUp.MsSql.AlwaysRunFirst.0000.SetRestrictedUserMode.sql", true, false, true, false)]
        [TestCase("DotStat.DbUp.MsSql.AlwaysRunLast.1234.TestScript.sql", false, false, false, false)]
        [TestCase("DotStat.DbUp.MsSql.AlwaysRunFirst.1234.TestScript.sql", true, true, true, false)]
        public void TestAlwaysRunFirstScript(string fileName, bool expectedResultDataDb, bool expectedResultMappingStoreDb, bool expectedResultCommonDb, bool expectedResultWithoutDba)

        {
            Assert.AreEqual(expectedResultDataDb,
                DbUp.Program.FilterAlwaysRunFirstScript(fileName, _upgradeOptionWithoutROloginNameDataDb), $"DataDb -> {fileName}");

            Assert.AreEqual(expectedResultMappingStoreDb,
                DbUp.Program.FilterAlwaysRunFirstScript(fileName, _upgradeOptionWithoutROloginNameMappingStoreDb), $"MappingStoreDb -> {fileName}");

            Assert.AreEqual(expectedResultCommonDb,
                DbUp.Program.FilterAlwaysRunFirstScript(fileName, _upgradeOptionWithoutROloginNameCommonDb), $"CommonDb -> {fileName}");

            Assert.AreEqual(expectedResultDataDb,
                DbUp.Program.FilterAlwaysRunFirstScript(fileName, _upgradeOptionWithROloginNameDataDb), $"DataDb -> {fileName}");

            Assert.AreEqual(expectedResultMappingStoreDb,
                DbUp.Program.FilterAlwaysRunFirstScript(fileName, _upgradeOptionWithROloginNameMappingStoreDb), $"MappingStoreDb -> {fileName}");

            Assert.AreEqual(expectedResultCommonDb,
                DbUp.Program.FilterAlwaysRunFirstScript(fileName, _upgradeOptionWithROloginNameCommonDb), $"CommonDb -> {fileName}");

            Assert.AreEqual(expectedResultWithoutDba,
                DbUp.Program.FilterAlwaysRunFirstScript(fileName, _upgradeOptionWithCommonDbWithoutDbaScripts), $"CommonDb -> {fileName}");
        }

        [Test]
        public void TestGetScriptPath()
        {
            Assert.AreEqual("DataDb",  DbUp.Program.GetScriptPath(_upgradeOptionWithROloginNameDataDb));

            Assert.AreEqual("DataDb", DbUp.Program.GetScriptPath(_upgradeOptionWithoutROloginNameDataDb));

            Assert.AreEqual("MappingStoreDb", DbUp.Program.GetScriptPath(_upgradeOptionWithROloginNameMappingStoreDb));

            Assert.AreEqual("MappingStoreDb", DbUp.Program.GetScriptPath(_upgradeOptionWithoutROloginNameMappingStoreDb));

            Assert.AreEqual("CommonDb", DbUp.Program.GetScriptPath(_upgradeOptionWithROloginNameCommonDb));

            Assert.AreEqual("CommonDb", DbUp.Program.GetScriptPath(_upgradeOptionWithoutROloginNameCommonDb));

        }

        [Test]
        public void GetScriptVariables()
        {
            ValidateScriptVariables(_upgradeOptionWithoutROloginNameDataDb);

            ValidateScriptVariables(_upgradeOptionWithoutROloginNameDataDb);

            ValidateScriptVariables(_upgradeOptionWithROloginNameMappingStoreDb);

            ValidateScriptVariables(_upgradeOptionWithoutROloginNameMappingStoreDb);

            ValidateScriptVariables(_upgradeOptionWithROloginNameCommonDb);

            ValidateScriptVariables(_upgradeOptionWithoutROloginNameCommonDb);
        }

        private void ValidateScriptVariables(UpgradeOption upgradeOption)
        {
            var scriptVariables = DbUp.Program.GetScriptVariables(upgradeOption);

            Assert.IsNotNull(scriptVariables);
            
            Assert.IsNotEmpty(scriptVariables);

            Assert.Contains("dbName", scriptVariables.Keys);
            Assert.Contains("loginName", scriptVariables.Keys);
            Assert.Contains("loginPwd", scriptVariables.Keys);
            Assert.Contains("alterPassword", scriptVariables.Keys);

            Assert.AreEqual( !upgradeOption.MappingStoreDb, scriptVariables.ContainsKey("dbVersion"));

            Assert.AreEqual(upgradeOption.IsROloginNameValid(), scriptVariables.ContainsKey("ROloginName"));
            Assert.AreEqual(upgradeOption.IsROloginNameValid(), scriptVariables.ContainsKey("ROloginPwd"));

            Assert.AreEqual(upgradeOption.LoginName, scriptVariables["loginName"]);
            Assert.AreEqual(upgradeOption.LoginPwd, scriptVariables["loginPwd"]);
            
            Assert.AreEqual(upgradeOption.AlterPassword ? "1" : "0", scriptVariables["alterPassword"]);
            
            if (!upgradeOption.MappingStoreDb)
            {
                var dbVersion = (upgradeOption.CommonDb
                    ? DatabaseVersion.CommonDbVersion
                    : DatabaseVersion.DataDbVersion).ToString();
                Assert.AreEqual(dbVersion, scriptVariables["dbVersion"]);
            }

            if (upgradeOption.IsROloginNameValid())
            {
                Assert.AreEqual(upgradeOption.ROloginName, scriptVariables["ROloginName"]);
                Assert.AreEqual(upgradeOption.ROloginPwd, scriptVariables["ROloginPwd"]);
            }

            var dbName = upgradeOption.DataDb ? "DataDb" :
                upgradeOption.MappingStoreDb ? "MappingStoreDb" :
                upgradeOption.CommonDb ? "CommonDb" : "N/A";

            Assert.AreEqual(dbName, scriptVariables["dbName"]);
        }
     }
}
