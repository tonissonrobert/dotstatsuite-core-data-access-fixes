﻿using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.CommonDb
{
    [SetUpFixture]
    internal sealed class Setup : BaseDbSetup
    {
        private readonly string _sqlConnectionString;
        
        public Setup()
        {
            _sqlConnectionString = Configuration.DotStatSuiteCoreCommonDbConnectionString;
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            CreateDb(_sqlConnectionString, "Common");
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            DeleteDb(_sqlConnectionString);
        }
    }
}
