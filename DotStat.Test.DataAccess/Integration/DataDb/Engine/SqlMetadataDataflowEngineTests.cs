﻿using System;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Engine.SqlServer;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.DataDb.Engine
{
    [TestFixture, Parallelizable(ParallelScope.Fixtures)]
    public class SqlMetadataDataflowEngineTests : BaseDbIntegrationTests
    {
        private SqlMetadataDataflowEngine _engine;
        private Domain.Dataflow _dataflow;

        public SqlMetadataDataflowEngineTests() : base("sdmx/CsvV2.xml")
        {
            _engine = new SqlMetadataDataflowEngine(Configuration);
            _dataflow = this.GetDataflow();

            InitMetadataForTests();
        }

        private void InitMetadataForTests()
        {
            var i = 100;

            var id = i++;
            _dataflow.Dsd.Msd.DbId = id;
            _dataflow.Dsd.MsdDbId = _dataflow.Dsd.Msd.DbId;

            foreach (var dim in _dataflow.Dsd.Dimensions)
                dim.DbId = i++;

            foreach (var attr in _dataflow.Dsd.Msd.MetadataAttributes)
                attr.DbId = i++;
        }

        [Test, Order(1)]
        public void No_Artifact_Record_Should_Exist()
        {
            Assert.ThrowsAsync<NotImplementedException>(async () => await _engine.GetDbId(_dataflow.Dsd.Msd.Code, _dataflow.Dsd.Msd.AgencyId, _dataflow.Dsd.Msd.Version, DotStatDb, CancellationToken));
        }

        [Test, Order(2)]
        public void Insert_Artifact()
        {
            Assert.ThrowsAsync<NotImplementedException>(async () => await _engine.InsertToArtefactTable(_dataflow, DotStatDb, CancellationToken));
        }

        [Test, Order(3)]
        public async Task Check_Msd_Meta_Tables_Created()
        {
            // Create MetadataFlow views ...

            Assert.IsFalse(await _engine.CreateDynamicDbObjects(_dataflow, DotStatDb, CancellationToken));

            var expectedViews = new string[]
            {
                $"{_dataflow.SqlMetadataDataFlowViewName('A')}",
                $"{_dataflow.SqlMetadataDataFlowViewName('B')}",
                $"{_dataflow.SqlDeletedMetadataDataFlowViewName('A')}",
                $"{_dataflow.SqlDeletedMetadataDataFlowViewName('B')}"
            };

            foreach (var view in expectedViews)
                //Views not created because the base table views are not created by this engine
                Assert.IsFalse(await DotStatDb.ViewExists(view, CancellationToken), $"View {view} have not been found");
        }

    }
}
