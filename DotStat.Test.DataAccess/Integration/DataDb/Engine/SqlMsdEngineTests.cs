﻿using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Engine.SqlServer;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.DataDb.Engine
{
    [TestFixture, Parallelizable(ParallelScope.Fixtures)]
    public class SqlMsdEngineTests : BaseDbIntegrationTests
    {
        private SqlMsdEngine _engine;
        private Domain.Dataflow _dataflow;

        public SqlMsdEngineTests() : base("sdmx/CsvV2.xml")
        {
            _engine = new SqlMsdEngine(Configuration);
            _dataflow = this.GetDataflow();

            InitMetadataForTests();
        }

        private void InitMetadataForTests()
        {
            var i = 100;

            foreach (var dim in _dataflow.Dsd.Dimensions)
                dim.DbId = i++;

            foreach (var attr in _dataflow.Dsd.Msd.MetadataAttributes)
                attr.DbId = i++;
            
        }

        [Test, Order(1)]
        public async Task No_Artifact_Record_Should_Exist()
        {
            var id = await _engine.GetDbId(_dataflow.Dsd.Msd.Code, _dataflow.Dsd.Msd.AgencyId, _dataflow.Dsd.Msd.Version, DotStatDb, CancellationToken);

            Assert.AreEqual(-1, id);
        }

        [Test, Order(2)]
        public async Task Insert_Artifact()
        {
            var id = await _engine.InsertToArtefactTable(_dataflow.Dsd.Msd, DotStatDb, CancellationToken);

            Assert.IsTrue(id > 0);

            _dataflow.Dsd.Msd.DbId = await _engine.GetDbId(_dataflow.Dsd.Msd.Code, _dataflow.Dsd.Msd.AgencyId, _dataflow.Dsd.Msd.Version, DotStatDb, CancellationToken);
            _dataflow.Dsd.MsdDbId = _dataflow.Dsd.Msd.DbId;

            Assert.AreEqual(_dataflow.Dsd.Msd.DbId, id);
        }

        [Test, Order(3)]
        public async Task Check_Msd_Meta_Tables_Created()
        {
            // Create Meta tables ...
            await _engine.CreateDynamicDbObjects(_dataflow.Dsd, DotStatDb, CancellationToken);

            var expectedTables = new string[]
            {
                $"{_dataflow.Dsd.SqlMetadataDataStructureTable('A')}",
                $"{_dataflow.Dsd.SqlMetadataDataStructureTable('B')}",
                $"{_dataflow.Dsd.SqlMetadataDataSetTable('A')}",
                $"{_dataflow.Dsd.SqlMetadataDataSetTable('B')}",
                $"{_dataflow.Dsd.SqlDeletedMetadataTable('A')}",
                $"{_dataflow.Dsd.SqlDeletedMetadataTable('B')}",
            };

            foreach (var table in expectedTables)
                Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");
            
            var expectedViews = new string[]
            {
                $"{_dataflow.Dsd.SqlMetaDataDsdViewName('A')}",
                $"{_dataflow.Dsd.SqlMetaDataDsdViewName('B')}",
                $"{_dataflow.Dsd.SqlDeletedMetadataDsdViewName('A')}",
                $"{_dataflow.Dsd.SqlDeletedMetadataDsdViewName('B')}"
            };

            foreach (var view in expectedViews)
                //Views not created because the base table views are not created by this engine
                Assert.IsFalse(await DotStatDb.ViewExists(view, CancellationToken), $"View {view} have not been found");

        }

    }
}
