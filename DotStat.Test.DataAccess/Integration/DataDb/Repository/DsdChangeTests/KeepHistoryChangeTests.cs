﻿using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Domain;
using DryIoc.ImTools;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.DataDb.Repository.DsdChangeTests
{
    [TestFixture, Parallelizable(ParallelScope.None)]
    [TestFixture("264D_264_SALDI2_ATTRIBUTE_TEST.xml", "sdmx", true)]
    [TestFixture("264D_264_SALDI2_ATTRIBUTE_TEST.xml", "sdmx", false)]
    [TestFixture("OECD-DF_TEST_DELETE-1.0-all_structures.xml", "sdmx", true)]
    [TestFixture("OECD-DF_TEST_DELETE-1.0-all_structures.xml", "sdmx", false)]
    public class KeepHistoryChangeTests : BaseDsdChangeTests
    {
        private IImportReferenceableStructure _originalStructure;
        private readonly string _fileName;
        private readonly string _folderName;
        private readonly bool _useDsdAsReferencedStructure;

        public KeepHistoryChangeTests(string fileName, string folderName, bool useDsdAsReferencedStructure)
        {
            _fileName = fileName;
            _folderName = folderName;
            _useDsdAsReferencedStructure = useDsdAsReferencedStructure;
        }

        [OneTimeSetUp]
        public async Task SetUp()
        {
            _originalStructure = await InitOriginalStructure(_fileName, _folderName, _useDsdAsReferencedStructure);
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_originalStructure);
        }

        [Test, Order(1)]
        public async Task CheckInitialDbObjectsTest()
        {
            Assert.DoesNotThrowAsync(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(_originalStructure, MsAccess, CancellationToken));

            var dsd = _originalStructure.Dsd;
            var expectedTables = new[]
            {
                dsd.SqlDsdAttrTable('A'),
                dsd.SqlDsdAttrTable('B'),
                dsd.SqlDimGroupAttrTable('A'),
                dsd.SqlDimGroupAttrTable('B')
            };

            foreach (var table in expectedTables)
                Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");

            var unExpectedTables = new[]
            {
                dsd.SqlDsdAttrHistoryTable('A'),
                dsd.SqlDsdAttrHistoryTable('B'),
                dsd.SqlDimGroupAttrHistoryTable('A'),
                dsd.SqlDimGroupAttrHistoryTable('B'),
            };

            foreach (var table in unExpectedTables)
                Assert.IsFalse(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} has been found");

            var expectedViews = new[]
            {
                dsd.SqlDataDsdViewName('A'),
                dsd.SqlDataDsdViewName('B')
            };

            foreach (var view in expectedViews)
                Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken), $"View {view} have not been found");

            if (_useDsdAsReferencedStructure)
                return;

            var dataFlow = _originalStructure as Dataflow;
            expectedViews = new[]
            {
                dataFlow.SqlDataDataFlowViewName('A'),
                dataFlow.SqlDataDataFlowViewName('B'),
                dataFlow.SqlDataReplaceDataFlowViewName('A'),
                dataFlow.SqlDataReplaceDataFlowViewName('B'),
                dataFlow.SqlDeletedDataViewName('A'),
                dataFlow.SqlDeletedDataViewName('B')
            };

            foreach (var view in expectedViews)
                Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken), $"View {view} have not been found");

            var unExpectedViews = new[]
            {
                dataFlow.SqlDataIncludeHistoryDataFlowViewName('A'),
                dataFlow.SqlDataIncludeHistoryDataFlowViewName('B')
            };

            foreach (var view in unExpectedViews)
                Assert.IsFalse(await DotStatDb.ViewExists(view, CancellationToken), $"View {view} has been found");
        }
        
        [Test, Order(2)]
        public async Task SetKeepHistoryOnChangeTest()
        {
            SetKeepHistory(true);
            Assert.DoesNotThrowAsync(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(_originalStructure, MsAccess, CancellationToken));

            var dsd = _originalStructure.Dsd;
            var expectedTables = new[]
            {
                dsd.SqlDsdAttrTable('A'),
                dsd.SqlDsdAttrTable('B'),
                dsd.SqlDimGroupAttrTable('A'),
                dsd.SqlDimGroupAttrTable('B')
            };

            if (_useDsdAsReferencedStructure)
            {
                expectedTables.Append(new[]
                {
                    dsd.SqlDsdAttrHistoryTable('A'),
                    dsd.SqlDsdAttrHistoryTable('B'),
                    dsd.SqlDimGroupAttrHistoryTable('A'),
                    dsd.SqlDimGroupAttrHistoryTable('B')
                });
            }

            foreach (var table in expectedTables)
                Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");

            var expectedViews = new[]
            {
                dsd.SqlDataDsdViewName('A'),
                dsd.SqlDataDsdViewName('B')
            };

            foreach (var view in expectedViews)
                Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken), $"View {view} have not been found");

            if (_useDsdAsReferencedStructure)
                return;

            var dataFlow = _originalStructure as Dataflow;
            expectedViews = new[]
            {
                dataFlow.SqlDataDataFlowViewName('A'),
                dataFlow.SqlDataDataFlowViewName('B'),
                dataFlow.SqlDataReplaceDataFlowViewName('A'),
                dataFlow.SqlDataReplaceDataFlowViewName('B'),
                dataFlow.SqlDeletedDataViewName('A'),
                dataFlow.SqlDeletedDataViewName('B'),
                dataFlow.SqlDataIncludeHistoryDataFlowViewName('A'),
                dataFlow.SqlDataIncludeHistoryDataFlowViewName('B')
            };

            foreach (var view in expectedViews)
                Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken), $"View {view} have not been found");
        }

        [Test, Order(3)]
        public async Task SetKeepHistoryOffChangeTest()
        {
            SetKeepHistory(false);

            Assert.DoesNotThrowAsync(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(_originalStructure, MsAccess, CancellationToken));

            var dsd = _originalStructure.Dsd;
            var expectedTables = new[]
            {
                dsd.SqlDsdAttrTable('A'),
                dsd.SqlDsdAttrTable('B'),
                dsd.SqlDimGroupAttrTable('A'),
                dsd.SqlDimGroupAttrTable('B')
            };

            if(_useDsdAsReferencedStructure){
                expectedTables.Append(new[]
                {
                    dsd.SqlDsdAttrHistoryTable('A'),
                    dsd.SqlDsdAttrHistoryTable('B'),
                    dsd.SqlDimGroupAttrHistoryTable('A'),
                    dsd.SqlDimGroupAttrHistoryTable('B')
                });

            }

            foreach (var table in expectedTables)
                Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");

            var expectedViews = new[]
            {
                dsd.SqlDataDsdViewName('A'),
                dsd.SqlDataDsdViewName('B')
            };

            foreach (var view in expectedViews)
                Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken), $"View {view} have not been found");

            if (_useDsdAsReferencedStructure)
                return;

            var dataFlow = _originalStructure as Dataflow;
            expectedViews = new[]
            {
                dataFlow.SqlDataDataFlowViewName('A'),
                dataFlow.SqlDataDataFlowViewName('B'),
                dataFlow.SqlDataReplaceDataFlowViewName('A'),
                dataFlow.SqlDataReplaceDataFlowViewName('B'),
                dataFlow.SqlDeletedDataViewName('A'),
                dataFlow.SqlDeletedDataViewName('B'),
                dataFlow.SqlDataIncludeHistoryDataFlowViewName('A'),
                dataFlow.SqlDataIncludeHistoryDataFlowViewName('B')
            };

            foreach (var view in expectedViews)
                Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken), $"View {view} have not been found");

        }
    }
}

