﻿using System.Collections.Generic;
using System.Linq;
using DotStat.Common.Enums;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;

namespace DotStat.Domain
{
    public class Codelist : MaintainableDotStatObject<ICodelistObject>
    {
        public IReadOnlyList<Code> Codes { get; private set; }

        public Codelist(ICodelistObject @base) : base(@base)
        {
            DbType = DbTypes.GetDbType(SDMXArtefactType.CodeList);
        }

        public void SetCodes(IEnumerable<Code> codes)
        {
            Codes = codes.ToArray();
        }
    }
}
