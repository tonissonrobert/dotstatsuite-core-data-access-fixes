﻿namespace DotStat.Domain
{
    public sealed class MappingSetParams
    {
        public string ActiveQuery { get; set; } 
        public string ReplaceQuery { get; set; } 
        public string DeleteQuery { get; set; } 
        public string IncludeHistoryQuery { get; set; } 
        public string OrderByTimeAsc { get; set; }
        public string OrderByTimeDesc { get; set; }
        public bool IsMetadata { get; set; }
    }
}
