﻿using System.Collections.Generic;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;

namespace DotStat.Domain
{
    public class Code : NameableDotStatObject<ICode>
    {
        public Codelist Codelist { get; protected set; }
        public IList<Code> ChildCodes { get; protected set; }
        public Code ParentCode { get; protected set; }
        public IEnumerable<Code> AllDescendents
        {
            get
            {
                foreach (var cm in ChildCodes)
                {
                    yield return cm;

                    foreach (var ccm in cm.AllDescendents)
                        yield return ccm;
                }
            }
        }

        public Code(Codelist codelist, ICode @base) : base(@base)
        {
            Codelist    = codelist;
            ChildCodes  = new List<Code>();
        }

        public void AddChildCode(Code code)
        {
            code.ParentCode = this;
            ChildCodes.Add(code);
        }

        public static string SanitizeMemberCode(string member)
        {
            throw new System.NotImplementedException();
        }
    }
}
